﻿namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    partial class ribbonComposeMessage : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ribbonComposeMessage()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabNewMailMessage = this.Factory.CreateRibbonTab();
            this.groupFastman = this.Factory.CreateRibbonGroup();
            this.buttonValidateLinks = this.Factory.CreateRibbonToggleButton();
            this.tabNewMailMessage.SuspendLayout();
            this.groupFastman.SuspendLayout();
            // 
            // tabNewMailMessage
            // 
            this.tabNewMailMessage.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabNewMailMessage.ControlId.OfficeId = "TabNewMailMessage";
            this.tabNewMailMessage.Groups.Add(this.groupFastman);
            this.tabNewMailMessage.Label = "TabNewMailMessage";
            this.tabNewMailMessage.Name = "tabNewMailMessage";
            // 
            // groupFastman
            // 
            this.groupFastman.Items.Add(this.buttonValidateLinks);
            this.groupFastman.Label = "Fastman";
            this.groupFastman.Name = "groupFastman";
            // 
            // buttonValidateLinks
            // 
            this.buttonValidateLinks.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonValidateLinks.Image = global::Fastman.Opentext.ContentServer.OutlookLinksValidator.Properties.Resources.ValidateLinksIcon;
            this.buttonValidateLinks.Label = "Validate Links";
            this.buttonValidateLinks.Name = "buttonValidateLinks";
            this.buttonValidateLinks.ShowImage = true;
            this.buttonValidateLinks.SuperTip = "Use to validate if recipients of email have read permissions to the Content Serve" +
    "r links in the email.";
            this.buttonValidateLinks.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonValidateLinks_Click);
            // 
            // ribbonComposeMessage
            // 
            this.Name = "ribbonComposeMessage";
            this.RibbonType = "Microsoft.Outlook.Mail.Compose";
            this.Tabs.Add(this.tabNewMailMessage);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.ribbonReadMessage_Load);
            this.tabNewMailMessage.ResumeLayout(false);
            this.tabNewMailMessage.PerformLayout();
            this.groupFastman.ResumeLayout(false);
            this.groupFastman.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabNewMailMessage;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupFastman;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton buttonValidateLinks;
    }

    partial class ThisRibbonCollection
    {
        internal ribbonComposeMessage ribbonComposeMessage
        {
            get { return this.GetRibbon<ribbonComposeMessage>(); }
        }
    }
}
