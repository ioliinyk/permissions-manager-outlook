﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Fastman.Opentext.ContentServer
{
    [DataContract]
    public class OTDSURLInfo
    {
        [DataMember(Name = "OTDSURL")]
        public string OTDSURL { get; set; }
    }

    [DataContract]
    public class ResourceIDInfo
    {
        [DataMember(Name = "ResourceID")]
        public string ResourceID { get; set; }
    }

    [DataContract]
    public class AuthenticationInfo
    {
        [DataMember(Name = "ticket")]
        public string ticket { get; set; }
    }

    #region PM REST objects
    [DataContract]
    public class RequestInfo
    {
        [DataMember(Name = "items")]
        public ItemData[] items { get; set; }
        [DataMember(Name = "users")]
        public UserData[] users { get; set; }
        [DataMember(Name = "language")]
        public string language { get; set; }
    }

    [DataContract]
    public class ResponseInfo
    {
        [DataMember(Name = "response_rows", EmitDefaultValue = false)]
        public ResponseRow[] response_rows { get; set; }
    }

    [DataContract]
    public class ResponseRow
    {
        [DataMember(Name = "item", EmitDefaultValue = false)]
        public ItemData item { get; set; }
        [DataMember(Name = "user", EmitDefaultValue = false)]
        public UserData user { get; set; }
        [DataMember(Name = "permission", EmitDefaultValue = false)]
        public PermissionData permission { get; set; }
    }

    [DataContract]
    public class ItemData
    {
        [DataMember(Name = "item_id", EmitDefaultValue = false)]
        public int item_id { get; set; }
        [DataMember(Name = "item_name", EmitDefaultValue = false)]
        public string item_name { get; set; }
        [DataMember(Name = "item_link", EmitDefaultValue = false)]
        public string item_link { get; set; }
        [DataMember(Name = "item_found", EmitDefaultValue = false)]
        public bool item_found { get; set; }
        [DataMember(Name = "item_matched_by", EmitDefaultValue = false)]
        public string item_matched_by { get; set; }
        [DataMember(Name = "item_type", EmitDefaultValue = false)]
        public int item_type { get; set; }
    }

    [DataContract]
    public class UserData
    {
        [DataMember(Name = "user_id", EmitDefaultValue = false)]
        public int user_id { get; set; }
        [DataMember(Name = "user_name", EmitDefaultValue = false)]
        public string user_name { get; set; }
        [DataMember(Name = "user_email", EmitDefaultValue = false)]
        public string user_email { get; set; }
        [DataMember(Name = "user_first_name", EmitDefaultValue = false)]
        public string user_first_name { get; set; }
        [DataMember(Name = "user_last_name", EmitDefaultValue = false)]
        public string user_last_name { get; set; }
        [DataMember(Name = "user_found", EmitDefaultValue = false)]
        public bool user_found { get; set; }
        [DataMember(Name = "user_privileges", EmitDefaultValue = false)]
        public int user_privileges { get; set; }
        [DataMember(Name = "user_matched_by", EmitDefaultValue = false)]
        public string user_matched_by { get; set; }
    }

    [DataContract]
    public class PermissionData
    {
        [DataMember(Name = "has_access", EmitDefaultValue = false)]
        public bool has_access { get; set; }
        [DataMember(Name = "has_clearance", EmitDefaultValue = false)]
        public bool has_clearance { get; set; }
        [DataMember(Name = "has_markings", EmitDefaultValue = false)]
        public bool has_markings { get; set; }
        [DataMember(Name = "has_permissions", EmitDefaultValue = false)]
        public bool has_permissions { get; set; }
        [DataMember(Name = "permissions", EmitDefaultValue = false)]
        public int permissions { get; set; }
        [DataMember(Name = "return_message", EmitDefaultValue = false)]
        public string return_message { get; set; }
    }

    #endregion


    #region CS REST objects

    [DataContract]
    public class InfoResult
    {
        [DataMember(Name = "addable_types")]
        public AddableTypes addable_types { get; set; }
        [DataMember(Name = "available_actions")]
        public AvailableActions available_actions { get; set; }
        [DataMember(Name = "available_roles"),]
        public AvailableRoles available_roles { get; set; }
        [DataMember(Name = "data")]
        public Data data { get; set; }
        [DataMember(Name = "definitions")]
        public Definitions definitions { get; set; }
        [DataMember(Name = "definitions_base")]
        public Array definitions_base { get; set; }
        [DataMember(Name = "definitions_order")]
        public Array definitions_order { get; set; }
        [DataMember(Name = "type")]
        public int type { get; set; }
        [DataMember(Name = "type_info")]
        public TypeInfo type_info { get; set; }
        [DataMember(Name = "type_name")]
        public string type_name { get; set; }
    }

    [DataContract]
    public class AddableTypes
    {
        [DataMember(Name = "icon")]
        public string icon { get; set; }
        [DataMember(Name = "type")]
        public int type { get; set; }
        [DataMember(Name = "type_name")]
        public string type_name { get; set; }
    }

    [DataContract]
    public class AvailableActions
    {
        [DataMember(Name = "parameterless")]
        public bool parameterless { get; set; }
        [DataMember(Name = "read_only")]
        public bool read_only { get; set; }
        [DataMember(Name = "type")]
        public string type { get; set; }
        [DataMember(Name = "type_name")]
        public string type_name { get; set; }
        [DataMember(Name = "webnode_signature")]
        public string webnode_signature { get; set; }
    }

    [DataContract]
    public class AvailableRoles
    {
        [DataMember(Name = "type")]
        public string type { get; set; }
        [DataMember(Name = "type_name")]
        public string type_name { get; set; }
    }

    [DataContract]
    public class Data
    {
        [DataMember(Name = "container")]
        public bool container { get; set; }
        [DataMember(Name = "create_date")]
        public string create_date { get; set; }
        [DataMember(Name = "create_user_id")]
        public int create_user_id { get; set; }
        [DataMember(Name = "description")]
        public string description { get; set; }
        [DataMember(Name = "description_multilingual")]
        public DescriptionMultilingual description_multilingual { get; set; }
        [DataMember(Name = "guid")]
        public string guid { get; set; }
        [DataMember(Name = "icon")]
        public string icon { get; set; }
        [DataMember(Name = "icon_large")]
        public string icon_large { get; set; }
        [DataMember(Name = "id")]
        public int id { get; set; }
        [DataMember(Name = "modify_date")]
        public string modify_date { get; set; }
        [DataMember(Name = "modify_user_id")]
        public int modify_user_id { get; set; }
        [DataMember(Name = "name")]
        public string name { get; set; }
        [DataMember(Name = "name_multilingual")]
        public NameMultilingual name_multilingual { get; set; }
        [DataMember(Name = "original_id")]
        public int original_id { get; set; }
        [DataMember(Name = "owner_group_id")]
        public int owner_group_id { get; set; }
        [DataMember(Name = "owner_user_id")]
        public int owner_user_id { get; set; }
        [DataMember(Name = "parent_id")]
        public int parent_id { get; set; }
        [DataMember(Name = "reserved")]
        public bool reserved { get; set; }
        [DataMember(Name = "reserved_date")]
        public string reserved_date { get; set; }
        [DataMember(Name = "reserved_user_id")]
        public int reserved_user_id { get; set; }
        [DataMember(Name = "volume_id")]
        public int volume_id { get; set; }
    }

    [DataContract]
    public class DescriptionMultilingual
    {
        [DataMember(Name = "en")]
        public string en { get; set; }
        [DataMember(Name = "de")]
        public string de { get; set; }
    }

    [DataContract]
    public class NameMultilingual
    {
        [DataMember(Name = "en")]
        public string en { get; set; }
        [DataMember(Name = "de")]
        public string de { get; set; }
    }

    [DataContract]
    public class Definitions
    {
    }

    [DataContract]
    public class TypeInfo
    {
        [DataMember(Name = "advanced_versioning")]
        public bool advanced_versioning { get; set; }
        [DataMember(Name = "container")]
        public bool container { get; set; }
    }
#endregion
}
