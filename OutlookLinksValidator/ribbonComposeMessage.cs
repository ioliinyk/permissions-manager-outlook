﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using Microsoft.Office.Tools;


namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    public partial class ribbonComposeMessage
    {
        private void ribbonReadMessage_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void buttonValidateLinks_Click(object sender, RibbonControlEventArgs e)
        {
            Outlook.Inspector inspector = (Outlook.Inspector)e.Control.Context;
            InspectorWrapper inspectorWrapper = Globals.ThisAddIn.InspectorWrappers[inspector];
            Microsoft.Office.Tools.CustomTaskPane pmLinksTaskPane = inspectorWrapper.PMLinksTaskPane;
            if (pmLinksTaskPane != null)
            {
                pmLinksTaskPane.Visible = ((RibbonToggleButton)sender).Checked;
                ((PMLinksTaskPaneControl)pmLinksTaskPane.Control).DoProcess(pmLinksTaskPane.Visible);
            }
        }


        //private Outlook.MailItem GetMailItem(RibbonControlEventArgs e)
        //{
        //    // Check to see if a item is select in explorer or we are in inspector.
        //    if (e.Control.Context is Outlook.Inspector)
        //    {
        //        Outlook.Inspector inspector = (Outlook.Inspector)e.Control.Context;

        //        if (inspector.CurrentItem is Outlook.MailItem)
        //        {
        //            return inspector.CurrentItem as Outlook.MailItem;
        //        }
        //        if (inspector != null) Marshal.ReleaseComObject(inspector);
        //    }

        //    if (e.Control.Context is Outlook.Explorer)
        //    {
        //        Outlook.Explorer explorer = (Outlook.Explorer)e.Control.Context;

        //        Outlook.Selection selectedItems = explorer.Selection;
        //        if (selectedItems.Count != 1)
        //        {
        //            return null;
        //        }

        //        if (selectedItems[1] is Outlook.MailItem)
        //        {
        //            return selectedItems[1] as Outlook.MailItem;
        //        }
        //        if (explorer != null) Marshal.ReleaseComObject(explorer);
        //    }

        //    return null;
        //}
    }
}
