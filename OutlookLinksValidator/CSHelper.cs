﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using Microsoft.Office.Tools;
using System.ComponentModel;
using System.Collections.Specialized;
using System.ServiceModel;


namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    public class CSHelper
    {
        public enum AuthenticationMode
        {
            [Description("Content Server authenticathion with prompt for user account")]
            CS_PROMPT = 1,
            [Description("Single Sign-On authenticathion")]
            SSO_IIS = 2,
            [Description("OTDS authenticathion")]
            OTDS = 3
        }

        private static ListDictionary _tokens = new ListDictionary();
        private static string Login(string url, AuthenticationMode authentication, int timeout)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("CSHelper:Login IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
            if (_tokens != null && _tokens[url] != null) return _tokens[url].ToString();
            string result = null;
            LoginForm login = null;
            Uri uri = null;
            switch (authentication)
            {
                case AuthenticationMode.CS_PROMPT:
                    login = new LoginForm();
                    login.Text = "Content Server Login";
                    login.labelInstructions.Text = "Enter Content Server User name and password";
                    uri = new Uri(url);
                    login.labelServer.Text = "Login to: " + uri.Host;
                    login.ShowDialog();
                    if (login.Cancelled)
                    {
                        login.Close();
                        return "Login cancelled.";
                    }
                    result = DoLogin(url, string.Format("func=ll.login&Username={0}&Password={1}&CurrentClientTime=D/{2}/{3}/:{4}:{5}:{6}", login.UserName.Text, login.Password.Text, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second), timeout);
                    login.Close();
                    break;
                case AuthenticationMode.SSO_IIS:
                    result = DoLogin(url, string.Format("func=ll.login&CurrentClientTime=D/{0}/{1}/:{2}:{3}:{4}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second), timeout);
                    break;
                case AuthenticationMode.OTDS:
                    result = DoOTDSLogin(url, true, timeout);
                    if (result == null || result == string.Empty)
                    {
                        result = DoOTDSLogin(url, false, timeout);
                    }
                    break;
                default:
                    break;
            }
            System.Diagnostics.Debug.WriteLine(string.Format("CSHelper:Login OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
            return result;

        }
        private static string DoLogin(string url, string postData, int timeout)
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(postData);
            CookieContainer cookieContainer = new CookieContainer();
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Timeout = timeout;
            request.PreAuthenticate = true;
            request.UseDefaultCredentials = true;
            request.KeepAlive = true;
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ServicePoint.Expect100Continue = false;
            request.Method = "POST";
            request.CookieContainer = cookieContainer;
            request.AllowAutoRedirect = false;
            request.ContentLength = data.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();
            request.GetResponse().Close();
           
            string result = "Error logging in.";
            
            try
            {
                result = cookieContainer.GetCookies(new Uri(url))["LLCookie"].Value.ToString();
                if (result != null)
                {
                    KeyValuePair<string, string> t = new KeyValuePair<string, string>("OTCSTICKET", System.Web.HttpUtility.UrlDecode(result));
                    if (!_tokens.Contains(url))
                    {
                        _tokens.Add(url, t);
                    }
                    else
                    {
                        _tokens[url] = t;
                    }
                }
            }
            catch
            {

            }
            return System.Web.HttpUtility.UrlDecode(result);
        }
        private static string DoOTDSLogin(string url, bool sso, int timeout)
        {
            //get otds ws url
            HttpWebRequest request = WebRequest.Create(string.Format("{0}?func=OTDSIntegration.GetServerURL", url)) as HttpWebRequest;
            request.Timeout = timeout;
            request.Method = "GET";
            OTDSURLInfo otdsURLInfo = null;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(OTDSURLInfo));
                    otdsURLInfo = jsonSerializer.ReadObject(response.GetResponseStream()) as OTDSURLInfo;
                }
            }
            catch
            {
                return "Could not connect to OTDS.";
            }
            ////get otds resourceID
            //request = WebRequest.Create(string.Format("{0}?func=OTDSIntegration.GetResourceID", url)) as HttpWebRequest;
            //request.Timeout = timeout;
            //request.Method = "GET";
            //ResourceIDInfo resourceIDInfo = null;
            //using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            //{
            //    System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(ResourceIDInfo));
            //    resourceIDInfo = jsonSerializer.ReadObject(response.GetResponseStream()) as ResourceIDInfo;
            //}

            //call otds authentication ws
            Uri uri = new Uri(string.Format("{0}/ot-authws/services/Authentication?wsdl", otdsURLInfo.OTDSURL));
            EndpointAddress address = new EndpointAddress(uri.ToString());
            OTDSAuthentication.AuthenticationClient otdsClient;

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.OpenTimeout = TimeSpan.FromMilliseconds(timeout);
            binding.ReceiveTimeout = TimeSpan.FromMilliseconds(timeout);
            binding.CloseTimeout = TimeSpan.FromMilliseconds(timeout);
            binding.SendTimeout = TimeSpan.FromMilliseconds(timeout);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;

            if (String.Equals(uri.Scheme.ToLower(), "http"))
            {
                binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            }
            else if (String.Equals(uri.Scheme.ToLower(), "https"))
            {
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
            }

            otdsClient = new OTDSAuthentication.AuthenticationClient(binding, address);

            string result;

            //string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //username = "dimitar.belyovski@fastman.com"; //username.Substring(username.IndexOf(@"\") + 1);
            OTDSAuthentication.OTAuthentication OTDSAuthHeader = new OTDSAuthentication.OTAuthentication();

            if (sso)
            {
                try
                {
                    result = otdsClient.AuthenticateCurrentUser(ref OTDSAuthHeader);
                }
                catch
                {
                    result = null;
                }
            }
            else
            {
                LoginForm login = new LoginForm();
                login.Text = "OTDS Login";
                login.labelInstructions.Text = "Enter OTDS User name and password";
                uri = new Uri(url);
                login.labelServer.Text = "Login to: " + uri.Host;
                login.ShowDialog();
                if (login.Cancelled)
                {
                    login.Close();
                    return "Login cancelled.";
                }
                try
                {
                    result = otdsClient.Authenticate(ref OTDSAuthHeader, login.UserName.Text, login.Password.Text);
                }
                catch(Exception ex)
                {
                    return ex.Message;
                }
                login.Close();
            }
            //string resourceID = otdsClient.GetResourceId(ref OTDSAuthHeader);
            //result = otdsClient.GetTicketForUser(ref OTDSAuthHeader, username, resourceID);
            if (result != null)
            {
                KeyValuePair<string, string> t = new KeyValuePair<string, string>("OTDSTICKET", System.Web.HttpUtility.UrlDecode(result));
                if (!_tokens.Contains(url))
                {
                    _tokens.Add(url, t);
                }
                else
                {
                    _tokens[url] = t;
                }
            }

            return System.Web.HttpUtility.UrlDecode(result);
        }
        private static int _callCounter = 0;
        public static dynamic FcsEffectivePerms(string url, Type requestType, Type returnType, dynamic postData, AuthenticationMode authentication, int timeout)
        {
            dynamic result = null;
            if (_tokens[url] == null)
            {
                result = Login(url, authentication, timeout);
                if (_tokens[url] == null) return result;
            }

            System.Diagnostics.Debug.WriteLine(string.Format("CSHelper:FcsEffectivePerms IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
            string requestURL = string.Format(@"{0}/api/v1/fcseffectiveperms", url);
            HttpWebRequest request = WebRequest.Create(new Uri(requestURL)) as HttpWebRequest;
            request.Timeout = timeout;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add(((KeyValuePair<string, string>)_tokens[url]).Key, ((KeyValuePair<string, string>)_tokens[url]).Value);
            request.PreAuthenticate = true;
            request.UseDefaultCredentials = true;

            System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(requestType);
            MemoryStream memoryStream = new MemoryStream();
            jsonSerializer.WriteObject(memoryStream, postData);
            string jsonString = Encoding.UTF8.GetString(memoryStream.ToArray());
            byte[] formData = UTF8Encoding.UTF8.GetBytes(HttpUtility.UrlEncode(string.Format("body={0},", jsonString)));
            //byte[] formData = HttpUtility.UrlEncodeToBytes(memoryStream.ToArray());
            request.ContentLength = formData.Length;
            try
            {
                using (Stream post = request.GetRequestStream())
                {
                    post.Write(formData, 0, formData.Length);
                }

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(returnType);
                    result = jsonSerializer.ReadObject(response.GetResponseStream());
                }
            }
            catch(System.Runtime.Serialization.SerializationException sex)
            {
                result = "Invalid response from server";
            }
            catch (System.Net.WebException ex)
            {
                if (_tokens[url] != null) _tokens[url] = null;
                if (authentication == AuthenticationMode.SSO_IIS && ex.Message.Contains("(401) Unauthorized"))
                {
                    if (_callCounter < 2)
                    {
                        _callCounter = _callCounter + 1;
                        FcsEffectivePerms(url, requestType, returnType, postData, authentication, timeout);
                    }
                    else
                    {
                        result = ex.Message;
                    }
                    _callCounter = 0;
                }
                else
                {
                    result = ex.Message;
                }
            }
            
            System.Diagnostics.Debug.WriteLine(string.Format("CSHelper:FcsEffectivePerms OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
            return result;
        }

        //public static dynamic HttpGet(string url, string token, Type returnType)
        //{
        //    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.Headers.Add("OTCSTICKET", token);
        //    request.PreAuthenticate = true;
        //    request.UseDefaultCredentials = true;

        //    dynamic result = null;
        //    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //    {
        //        System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(returnType);
        //        result = jsonSerializer.ReadObject(response.GetResponseStream());
        //    }

        //    return result;
        //}

        //public static string HttpPost(string url, string[] paramName, string[] paramVal)
        //{
        //    HttpWebRequest request = WebRequest.Create(new Uri(url)) as HttpWebRequest;
        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.PreAuthenticate = true;
        //    request.UseDefaultCredentials = true;

        //    // Build a string with all the params, properly encoded.
        //    // We assume that the arrays paramName and paramVal are
        //    // of equal length:
        //    StringBuilder paramz = new StringBuilder();
        //    for (int i = 0; i < paramName.Length; i++)
        //    {
        //        paramz.Append(paramName[i]);
        //        paramz.Append("=");
        //        paramz.Append(HttpUtility.UrlEncode(paramVal[i]));
        //        paramz.Append("&");
        //    }

        //    // Encode the parameters as form data:
        //    byte[] formData = UTF8Encoding.UTF8.GetBytes(paramz.ToString());
        //    request.ContentLength = formData.Length;

        //    // Send the request:
        //    using (Stream post = request.GetRequestStream())
        //    {
        //        post.Write(formData, 0, formData.Length);
        //    }

        //    // Pick up the response:
        //    string result = null;
        //    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //    {
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        result = reader.ReadToEnd();
        //    }

        //    return result;
        //}
    }
}
