﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    public class InspectorWrapper
    {
        private Outlook.Inspector inspector;
        private Microsoft.Office.Tools.CustomTaskPane pmLinksTaskPane;

        public InspectorWrapper(Outlook.Inspector Inspector)
        {
            inspector = Inspector;
            ((Outlook.InspectorEvents_Event)inspector).Close +=  new Outlook.InspectorEvents_CloseEventHandler(InspectorWrapper_Close);
            PMLinksTaskPaneControl pmLinksTaskPaneControl  = new PMLinksTaskPaneControl(inspector.CurrentItem);
            pmLinksTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(pmLinksTaskPaneControl, "Fastman Permissions Manager for Outlook", inspector);
            pmLinksTaskPane.Width = ((Microsoft.Office.Interop.Outlook.Inspector)pmLinksTaskPane.Window).Width / 2;
            pmLinksTaskPane.VisibleChanged += new EventHandler(TaskPane_VisibleChanged);
            pmLinksTaskPaneControl.Resize += new EventHandler(pmLinksTaskPaneControl_Resize);
        }

        private void pmLinksTaskPaneControl_Resize(object sender, EventArgs e)
        {
         //  if (pmLinksTaskPane.Width > ((Microsoft.Office.Interop.Outlook.Inspector)pmLinksTaskPane.Window).Width / 2) pmLinksTaskPane.Width = ((Microsoft.Office.Interop.Outlook.Inspector)pmLinksTaskPane.Window).Width / 2;
        }

        private void TaskPane_VisibleChanged(object sender, EventArgs e)
        {
            Globals.Ribbons[inspector].ribbonComposeMessage.buttonValidateLinks.Checked = pmLinksTaskPane.Visible;
        }

        private void InspectorWrapper_Close()
        {
            if (pmLinksTaskPane != null)
            {
                Globals.ThisAddIn.CustomTaskPanes.Remove(pmLinksTaskPane);
            }

            pmLinksTaskPane = null;
            Globals.ThisAddIn.InspectorWrappers.Remove(inspector);
            ((Outlook.InspectorEvents_Event)inspector).Close -= new Outlook.InspectorEvents_CloseEventHandler(InspectorWrapper_Close);
            inspector = null;
        }

        public Microsoft.Office.Tools.CustomTaskPane PMLinksTaskPane
        {
            get
            {
                return pmLinksTaskPane;
            }
        }
    }

    public partial class ThisAddIn
    {
        private Dictionary<Outlook.Inspector, InspectorWrapper> inspectorWrappersValue = new Dictionary<Outlook.Inspector, InspectorWrapper>();
        private Outlook.Inspectors inspectors;
        protected override Office.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            Outlook.Application app = this.GetHostItem<Outlook.Application>(typeof(Outlook.Application), "Application");
            int lcid = app.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDUI);
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lcid);
            return base.CreateRibbonExtensibilityObject();
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(this.Application.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDUI));
            inspectors = this.Application.Inspectors;
            inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);

            foreach (Outlook.Inspector inspector in inspectors)
            {
                Inspectors_NewInspector(inspector);
            }
            //Application.ItemSend += new Outlook.ApplicationEvents_11_ItemSendEventHandler(Application_ItemSend);
            //Application.OptionsPagesAdd += new Outlook.ApplicationEvents_11_OptionsPagesAddEventHandler(Application_OptionsPagesAdd);
        }
        //private void Application_OptionsPagesAdd(Outlook.PropertyPages Pages)
        //{
        //    Pages.Add(new AddInOptionsControl(), "Fastman");
        //}

        //private void Application_ItemSend(object Item, ref bool Cancel)
        //{
        //    throw new NotImplementedException();
        //}

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            inspectors.NewInspector -=  new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);
            inspectors = null;
            inspectorWrappersValue = null;
        }

        private void Inspectors_NewInspector(Outlook.Inspector Inspector)
        {
            if (Inspector.CurrentItem is Outlook.MailItem)
            {
                inspectorWrappersValue.Add(Inspector, new InspectorWrapper(Inspector));
            }
        }

        public Dictionary<Outlook.Inspector, InspectorWrapper> InspectorWrappers
        {
            get
            {
                return inspectorWrappersValue;
            }
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
