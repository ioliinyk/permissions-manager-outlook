﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    public class MatrixRow
    {
        private string _user;
        private string _department;
        private string _link;
        private string _linkaddress;
        private bool _hasaccess;
        private string _message;
        private bool _unchecked;
        private int _permissions;
        private string _action;

        public string User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }
        public string Department
        {
            get
            {
                return _department;
            }
            set
            {
                _department = value;
            }
        }
        public string Link
        {
            get
            {
                return _link;
            }
            set
            {
                _link = value;
            }
        }
        public string LinkAddress
        {
            get
            {
                return _linkaddress;
            }
            set
            {
                _linkaddress = value;
            }
        }

        public bool HasAccess
        {
            get
            {
                return _hasaccess;
            }
            set
            {
                _hasaccess = value;
            }
        }
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }
        public bool Unchecked
        {
            get
            {
                return _unchecked;
            }
            set
            {
                _unchecked = value;
            }
        }
        public int Permissions
        {
            get
            {
                return _permissions;
            }
            set
            {
                _permissions = value;
            }
        }
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
            }
        }
    }

}
