using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
	/// <summary>
	/// Summary description for LoginForm.
	/// </summary>
	public class LoginForm : System.Windows.Forms.Form
	{
		public System.Windows.Forms.Label Label2;
		public System.Windows.Forms.Label Label1;
		public System.Windows.Forms.Button Cancel;
		public System.Windows.Forms.Button Login;
		public System.Windows.Forms.TextBox UserName;
		public System.Windows.Forms.Label labelServer;
		public System.Windows.Forms.TextBox Password;
        public Label labelInstructions;
        public bool Cancelled = false;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		public LoginForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Cancel = new System.Windows.Forms.Button();
            this.Login = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.UserName = new System.Windows.Forms.TextBox();
            this.labelServer = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.TextBox();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Cancel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Cancel.Location = new System.Drawing.Point(128, 112);
            this.Cancel.Name = "Cancel";
            this.Cancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cancel.Size = new System.Drawing.Size(89, 25);
            this.Cancel.TabIndex = 27;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Login.Cursor = System.Windows.Forms.Cursors.Default;
            this.Login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Login.Location = new System.Drawing.Point(32, 112);
            this.Login.Name = "Login";
            this.Login.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Login.Size = new System.Drawing.Size(89, 25);
            this.Login.TabIndex = 26;
            this.Login.Text = "Login";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(17, 78);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(97, 17);
            this.Label2.TabIndex = 24;
            this.Label2.Text = "Password";
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(17, 54);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(97, 17);
            this.Label1.TabIndex = 22;
            this.Label1.Text = "User name";
            // 
            // UserName
            // 
            this.UserName.AcceptsReturn = true;
            this.UserName.BackColor = System.Drawing.SystemColors.Window;
            this.UserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.UserName.Location = new System.Drawing.Point(89, 54);
            this.UserName.MaxLength = 0;
            this.UserName.Name = "UserName";
            this.UserName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.UserName.Size = new System.Drawing.Size(129, 20);
            this.UserName.TabIndex = 23;
            // 
            // labelServer
            // 
            this.labelServer.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.labelServer.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelServer.Location = new System.Drawing.Point(17, 29);
            this.labelServer.Name = "labelServer";
            this.labelServer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelServer.Size = new System.Drawing.Size(197, 22);
            this.labelServer.TabIndex = 28;
            this.labelServer.Text = "Login To: fastman-dev01";
            this.labelServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Password
            // 
            this.Password.AcceptsReturn = true;
            this.Password.BackColor = System.Drawing.SystemColors.Window;
            this.Password.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Password.Location = new System.Drawing.Point(89, 78);
            this.Password.MaxLength = 0;
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Password.Size = new System.Drawing.Size(129, 20);
            this.Password.TabIndex = 25;
            // 
            // labelInstructions
            // 
            this.labelInstructions.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.labelInstructions.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstructions.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelInstructions.Location = new System.Drawing.Point(17, 8);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelInstructions.Size = new System.Drawing.Size(235, 22);
            this.labelInstructions.TabIndex = 29;
            this.labelInstructions.Text = "Enter Content Server User name and Password";
            this.labelInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.Login;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(248, 146);
            this.Controls.Add(this.labelInstructions);
            this.Controls.Add(this.labelServer);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Content Server Login";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void Login_Click(object sender, System.EventArgs e)
        {
            if (UserName.Text.Trim() != string.Empty && Password.Text.Trim() != string.Empty)
            {
                Cancelled = false;
                this.Hide();
            }
            else
            {
                MessageBox.Show("Please enter user name and password.", "Bulk Data Manager Error connecting...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            Cancelled = true;
        }
	}
}
