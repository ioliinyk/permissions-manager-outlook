﻿namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    partial class PMLinksTaskPaneControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(_email);
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelTop = new System.Windows.Forms.Panel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textStatus = new System.Windows.Forms.Label();
            this.buttonExport = new System.Windows.Forms.Button();
            this.buttonCheckLinks = new System.Windows.Forms.Button();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabLinks = new System.Windows.Forms.TabControl();
            this.tabInvalid = new System.Windows.Forms.TabPage();
            this.gridInvalidLinks = new System.Windows.Forms.DataGridView();
            this.tabValid = new System.Windows.Forms.TabPage();
            this.gridValidLinks = new System.Windows.Forms.DataGridView();
            this.tabUnchecked = new System.Windows.Forms.TabPage();
            this.gridUncheckedLinks = new System.Windows.Forms.DataGridView();
            this.panelTop.SuspendLayout();
            this.panelTab.SuspendLayout();
            this.tabLinks.SuspendLayout();
            this.tabInvalid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvalidLinks)).BeginInit();
            this.tabValid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridValidLinks)).BeginInit();
            this.tabUnchecked.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridUncheckedLinks)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.progressBar);
            this.panelTop.Controls.Add(this.textStatus);
            this.panelTop.Controls.Add(this.buttonExport);
            this.panelTop.Controls.Add(this.buttonCheckLinks);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(413, 29);
            this.panelTop.TabIndex = 1;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(308, 3);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 8;
            this.progressBar.Visible = false;
            // 
            // textStatus
            // 
            this.textStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textStatus.Location = new System.Drawing.Point(85, 5);
            this.textStatus.Name = "textStatus";
            this.textStatus.Size = new System.Drawing.Size(244, 20);
            this.textStatus.TabIndex = 5;
            this.textStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonExport
            // 
            this.buttonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExport.Location = new System.Drawing.Point(335, 3);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(75, 23);
            this.buttonExport.TabIndex = 1;
            this.buttonExport.Text = "Export";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Visible = false;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonCheckLinks
            // 
            this.buttonCheckLinks.Location = new System.Drawing.Point(3, 3);
            this.buttonCheckLinks.Name = "buttonCheckLinks";
            this.buttonCheckLinks.Size = new System.Drawing.Size(75, 23);
            this.buttonCheckLinks.TabIndex = 0;
            this.buttonCheckLinks.Text = "Refresh";
            this.buttonCheckLinks.UseVisualStyleBackColor = true;
            this.buttonCheckLinks.Click += new System.EventHandler(this.buttonCheckLinks_Click);
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabLinks);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 29);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(413, 266);
            this.panelTab.TabIndex = 4;
            // 
            // tabLinks
            // 
            this.tabLinks.Controls.Add(this.tabInvalid);
            this.tabLinks.Controls.Add(this.tabValid);
            this.tabLinks.Controls.Add(this.tabUnchecked);
            this.tabLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLinks.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabLinks.Location = new System.Drawing.Point(0, 0);
            this.tabLinks.Name = "tabLinks";
            this.tabLinks.SelectedIndex = 0;
            this.tabLinks.Size = new System.Drawing.Size(413, 266);
            this.tabLinks.TabIndex = 3;
            this.tabLinks.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabLinks_DrawItem);
            this.tabLinks.SelectedIndexChanged += new System.EventHandler(this.tabLinks_SelectedIndexChanged);
            // 
            // tabInvalid
            // 
            this.tabInvalid.BackColor = System.Drawing.SystemColors.Control;
            this.tabInvalid.Controls.Add(this.gridInvalidLinks);
            this.tabInvalid.ForeColor = System.Drawing.Color.Red;
            this.tabInvalid.Location = new System.Drawing.Point(4, 22);
            this.tabInvalid.Name = "tabInvalid";
            this.tabInvalid.Padding = new System.Windows.Forms.Padding(3);
            this.tabInvalid.Size = new System.Drawing.Size(405, 240);
            this.tabInvalid.TabIndex = 0;
            this.tabInvalid.Text = "Invalid Links";
            // 
            // gridInvalidLinks
            // 
            this.gridInvalidLinks.AllowUserToAddRows = false;
            this.gridInvalidLinks.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Red;
            this.gridInvalidLinks.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridInvalidLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridInvalidLinks.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridInvalidLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInvalidLinks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridInvalidLinks.Location = new System.Drawing.Point(3, 3);
            this.gridInvalidLinks.Name = "gridInvalidLinks";
            this.gridInvalidLinks.RowHeadersVisible = false;
            this.gridInvalidLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridInvalidLinks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridInvalidLinks.Size = new System.Drawing.Size(399, 234);
            this.gridInvalidLinks.TabIndex = 1;
            // 
            // tabValid
            // 
            this.tabValid.BackColor = System.Drawing.SystemColors.Control;
            this.tabValid.Controls.Add(this.gridValidLinks);
            this.tabValid.ForeColor = System.Drawing.Color.Green;
            this.tabValid.Location = new System.Drawing.Point(4, 22);
            this.tabValid.Name = "tabValid";
            this.tabValid.Padding = new System.Windows.Forms.Padding(3);
            this.tabValid.Size = new System.Drawing.Size(405, 240);
            this.tabValid.TabIndex = 1;
            this.tabValid.Text = "Valid Links";
            // 
            // gridValidLinks
            // 
            this.gridValidLinks.AllowUserToAddRows = false;
            this.gridValidLinks.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Green;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Green;
            this.gridValidLinks.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.gridValidLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Green;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Green;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridValidLinks.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridValidLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridValidLinks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridValidLinks.Location = new System.Drawing.Point(3, 3);
            this.gridValidLinks.Name = "gridValidLinks";
            this.gridValidLinks.RowHeadersVisible = false;
            this.gridValidLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridValidLinks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridValidLinks.Size = new System.Drawing.Size(399, 234);
            this.gridValidLinks.TabIndex = 0;
            // 
            // tabUnchecked
            // 
            this.tabUnchecked.BackColor = System.Drawing.SystemColors.Control;
            this.tabUnchecked.Controls.Add(this.gridUncheckedLinks);
            this.tabUnchecked.Location = new System.Drawing.Point(4, 22);
            this.tabUnchecked.Name = "tabUnchecked";
            this.tabUnchecked.Size = new System.Drawing.Size(405, 240);
            this.tabUnchecked.TabIndex = 2;
            this.tabUnchecked.Text = "Unchecked Links";
            // 
            // gridUncheckedLinks
            // 
            this.gridUncheckedLinks.AllowUserToAddRows = false;
            this.gridUncheckedLinks.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.gridUncheckedLinks.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gridUncheckedLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridUncheckedLinks.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridUncheckedLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUncheckedLinks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridUncheckedLinks.Location = new System.Drawing.Point(0, 0);
            this.gridUncheckedLinks.Name = "gridUncheckedLinks";
            this.gridUncheckedLinks.RowHeadersVisible = false;
            this.gridUncheckedLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridUncheckedLinks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridUncheckedLinks.Size = new System.Drawing.Size(405, 240);
            this.gridUncheckedLinks.TabIndex = 1;
            // 
            // PMLinksTaskPaneControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelTab);
            this.Controls.Add(this.panelTop);
            this.MinimumSize = new System.Drawing.Size(413, 295);
            this.Name = "PMLinksTaskPaneControl";
            this.Size = new System.Drawing.Size(413, 295);
            this.panelTop.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            this.tabLinks.ResumeLayout(false);
            this.tabInvalid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInvalidLinks)).EndInit();
            this.tabValid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridValidLinks)).EndInit();
            this.tabUnchecked.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridUncheckedLinks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button buttonCheckLinks;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Panel panelTab;
        private System.Windows.Forms.TabControl tabLinks;
        private System.Windows.Forms.TabPage tabInvalid;
        private System.Windows.Forms.DataGridView gridInvalidLinks;
        private System.Windows.Forms.TabPage tabValid;
        private System.Windows.Forms.DataGridView gridValidLinks;
        private System.Windows.Forms.Label textStatus;
        private System.Windows.Forms.TabPage tabUnchecked;
        private System.Windows.Forms.DataGridView gridUncheckedLinks;
        private System.Windows.Forms.ProgressBar progressBar;
        private SpannedDataGridView.DataGridViewTextBoxColumnSpanned columnInvalidLinksUser;
        private System.Windows.Forms.DataGridViewLinkColumn columnInvalidLinksLink;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnInvalidLinksPermission;
        private System.Windows.Forms.DataGridViewLinkColumn columnInvalidLinksAction;
        private SpannedDataGridView.DataGridViewTextBoxColumnSpanned columnValidLinksUser;
        private System.Windows.Forms.DataGridViewLinkColumn columnValidLinksLink;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnValidLinksPermission;
        private System.Windows.Forms.DataGridViewLinkColumn columnValidLinksAction;
        private System.Windows.Forms.DataGridViewLinkColumn columnUncheckedLinksLink;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnUncheckedLinksReason;

    }
}
