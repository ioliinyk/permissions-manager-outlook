﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Fastman Permissions Manager for Outlook")]
[assembly: AssemblyDescription("Validates if email recipients have effective read permissions to Content Server links in emails.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fastman")]
[assembly: AssemblyProduct("Fastman.Opentext.ContentServer.OutlookLinksValidator")]
[assembly: AssemblyCopyright("Copyright ©  Fastman 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("70c02093-9cfa-46ca-9fb3-127ac9099bad")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.1.0")]
[assembly: AssemblyFileVersion("1.2.1.0")]
