﻿namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    partial class AddInOptionsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridCSInstances = new Fastman.Opentext.ContentServer.OutlookLinksValidator.CustomDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coulmnAction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnTimeout = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCSAuthentication = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.columnCSInstance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridCSInstances)).BeginInit();
            this.SuspendLayout();
            // 
            // gridCSInstances
            // 
            this.gridCSInstances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCSInstances.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnName,
            this.columnCSInstance,
            this.columnCSAuthentication,
            this.columnTimeout,
            this.coulmnAction});
            this.gridCSInstances.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCSInstances.Location = new System.Drawing.Point(0, 0);
            this.gridCSInstances.Name = "gridCSInstances";
            this.gridCSInstances.RowHeadersWidth = 35;
            this.gridCSInstances.Size = new System.Drawing.Size(427, 296);
            this.gridCSInstances.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "URL";
            this.dataGridViewTextBoxColumn1.HeaderText = "Content Server URL";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "URL";
            this.dataGridViewTextBoxColumn2.HeaderText = "Content Server URL";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // coulmnAction
            // 
            this.coulmnAction.DataPropertyName = "Action";
            this.coulmnAction.HeaderText = "Action";
            this.coulmnAction.Name = "coulmnAction";
            // 
            // columnTimeout
            // 
            this.columnTimeout.DataPropertyName = "Timeout";
            this.columnTimeout.HeaderText = "Timeout";
            this.columnTimeout.Name = "columnTimeout";
            this.columnTimeout.Width = 40;
            // 
            // columnCSAuthentication
            // 
            this.columnCSAuthentication.DataPropertyName = "Authentication";
            this.columnCSAuthentication.HeaderText = "Authentication";
            this.columnCSAuthentication.Name = "columnCSAuthentication";
            this.columnCSAuthentication.Width = 80;
            // 
            // columnCSInstance
            // 
            this.columnCSInstance.DataPropertyName = "URL";
            this.columnCSInstance.HeaderText = "Content Server URL";
            this.columnCSInstance.Name = "columnCSInstance";
            this.columnCSInstance.Width = 200;
            // 
            // columnName
            // 
            this.columnName.DataPropertyName = "Name";
            this.columnName.HeaderText = "#";
            this.columnName.Name = "columnName";
            this.columnName.Visible = false;
            this.columnName.Width = 20;
            // 
            // AddInOptionsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridCSInstances);
            this.Name = "AddInOptionsControl";
            this.Size = new System.Drawing.Size(427, 296);
            ((System.ComponentModel.ISupportInitialize)(this.gridCSInstances)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomDataGridView gridCSInstances;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCSInstance;
        private System.Windows.Forms.DataGridViewComboBoxColumn columnCSAuthentication;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnTimeout;
        private System.Windows.Forms.DataGridViewTextBoxColumn coulmnAction;

    }
}
