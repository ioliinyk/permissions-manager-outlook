﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.AccessControl;
using Microsoft.Win32;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{

    public static class AddInSettings
    {
        private const string ADDINN_REGISTRY_KEY = @"Software\Fastman\Permissions Manager for Outlook\CS Instances";

        public class CSInstanceSettings
        {
            private string _name;
            private string _url;
            private CSHelper.AuthenticationMode _authentication;
            private int _timeout;
            private string _action;

            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }
            public string URL
            {
                get
                {
                    return _url;
                }
                set
                {
                    _url = value;
                }
            }
            public CSHelper.AuthenticationMode Authentication
            {
                get
                {
                    return _authentication;
                }
                set
                {
                    _authentication = value;
                }
            }
                  public int Timeout
            {
                get
                {
                    return _timeout;
                }
                set
                {
                    _timeout = value;
                }
            }
                  public string Action
            {
                get
                {
                    return _action;
                }
                set
                {
                    _action = value;
                }
            }
        }

        private static RegistryKey GetRegistry(RegistryHive hive)
        {
            //.NET app compiled for "x86":
            //Always 32-bit
            //On 32-bit platforms, accesses 32-bit registry
            //On 64-bit platforms, accesses 32-bit registry (inside Wow6432Node)
            //64-bit registry inaccessible (without doing something weird)
            //.NET app compiled for "x64":
            //Always 64 bit
            //On 32-bit platforms, won't run
            //On 64-bit platforms, accesses 64-bit registry (not inside Wow6432Node)
            //If you want to get to the 32-bit registry, you need to do something weird
            //.NET app compiled for "AnyCpu"
            //Either 32 or 64 bit depending on platform
            //On 32-bit platforms, accesses 32-bit registry
            //On 64-bit platforms, accesses 64-bit registry (not inside Wow6432Node)
            RegistryKey result = RegistryKey.OpenBaseKey(hive, Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32);
            return result;
        }

        private static List<CSInstanceSettings> ReadSettings(bool currentUser)
        {
            List<CSInstanceSettings> result = new List<CSInstanceSettings>();

            RegistryKey registryKey = currentUser ? GetRegistry(RegistryHive.CurrentUser).OpenSubKey(ADDINN_REGISTRY_KEY) : GetRegistry(RegistryHive.LocalMachine).OpenSubKey(ADDINN_REGISTRY_KEY);
            if (registryKey == null || registryKey.SubKeyCount == 0) return null;

            foreach (var subKeyName in registryKey.GetSubKeyNames())
            {
                CSInstanceSettings csInstance = new CSInstanceSettings();
                csInstance.Name = subKeyName;
                object url = registryKey.OpenSubKey(subKeyName).GetValue("URL");
                csInstance.URL = url == null ? string.Empty : url.ToString();
                object auth = registryKey.OpenSubKey(subKeyName).GetValue("Authentication");
                csInstance.Authentication = auth == null || auth.ToString() == string.Empty ? CSHelper.AuthenticationMode.CS_PROMPT : (CSHelper.AuthenticationMode)auth;
                object timeout = registryKey.OpenSubKey(subKeyName).GetValue("Timeout");
                csInstance.Timeout = timeout == null ? 5000 : Convert.ToInt32(timeout);
                object action = registryKey.OpenSubKey(subKeyName).GetValue("Action");
                csInstance.Action = action == null ? string.Empty : action.ToString();
                result.Add(csInstance);
            }
            registryKey.Close();
            return result;
        }

        public static List<CSInstanceSettings> CSInstances
        {
            get
            {
                List<CSInstanceSettings> csinstances = null;
                List<CSInstanceSettings> machineSettings = ReadSettings(false);
                List<CSInstanceSettings> userSettings = ReadSettings(true);
                if (machineSettings != null)
                {
                    if (csinstances == null) csinstances = new List<CSInstanceSettings>();
                    foreach (var machineSetting in machineSettings)
                    {
                        if (csinstances.Find(s => s.URL.ToLower() == machineSetting.URL.ToLower()) == null) csinstances.Add(machineSetting);
                    }
                }
                if (userSettings != null)
                {
                    if (csinstances == null) csinstances = new List<CSInstanceSettings>();
                    foreach (var userSetting in userSettings)
                    {
                        if (csinstances.Find(s => s.URL.ToLower() == userSetting.URL.ToLower()) == null) csinstances.Add(userSetting);
                    }
                }
                return csinstances;
            }
            set
            {
                RegistrySecurity registrySecurity = new RegistrySecurity();
                registrySecurity.AddAccessRule(new RegistryAccessRule(string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName), RegistryRights.FullControl, InheritanceFlags.None, PropagationFlags.None, AccessControlType.Allow));
                GetRegistry(RegistryHive.CurrentUser).DeleteSubKeyTree(ADDINN_REGISTRY_KEY);
                RegistryKey registryKey = GetRegistry(RegistryHive.CurrentUser).CreateSubKey(ADDINN_REGISTRY_KEY, RegistryKeyPermissionCheck.Default, registrySecurity);
                
                foreach (CSInstanceSettings csInstance in value)
                {
                    RegistryKey instanceRegistryKey = registryKey.OpenSubKey(csInstance.Name, true);
                    if (instanceRegistryKey == null) instanceRegistryKey = registryKey.CreateSubKey(csInstance.Name, RegistryKeyPermissionCheck.Default, registrySecurity);
                    instanceRegistryKey.SetValue("URL", csInstance.URL);
                    instanceRegistryKey.SetValue("Authentication", (int)csInstance.Authentication);
                    instanceRegistryKey.SetValue("Timeout", csInstance.Timeout);
                    instanceRegistryKey.SetValue("Action", csInstance.Action);
                }
            }
        }
    }
}
