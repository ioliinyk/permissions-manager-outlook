﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections;
using System.Security.AccessControl;
using Microsoft.Win32;
using Fastman.Opentext.ContentServer.OutlookLinksValidator.Properties;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    public partial class PMLinksTaskPaneControl : UserControl
    {
        private Outlook.MailItem _email = null;
        private BackgroundWorker _bw;
        private bool _paneCreated = false;

        public PMLinksTaskPaneControl(Outlook.MailItem eMail)
        {
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
            InitializeComponent();
            _email = eMail; //we release in Dispose
            tabInvalid.Text = Resources.tabInvalid;
            tabValid.Text = Resources.tabValid;
            tabUnchecked.Text = Resources.tabUnchecked;
            buttonExport.Text = Resources.buttonExport;
            buttonCheckLinks.Text = Resources.buttonRefresh;
            #region Create Invalid Links Grid
            gridInvalidLinks.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            gridInvalidLinks.CellContentClick -= new DataGridViewCellEventHandler(grid_CellContentClick);
            gridInvalidLinks.CellContentClick += new DataGridViewCellEventHandler(grid_CellContentClick);
            columnInvalidLinksUser = new SpannedDataGridView.DataGridViewTextBoxColumnSpanned();
            columnInvalidLinksUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnInvalidLinksUser.HeaderText = Resources.strRecipient;
            columnInvalidLinksUser.Name = "UserColumn";
            columnInvalidLinksUser.ReadOnly = true;
            columnInvalidLinksUser.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnInvalidLinksUser.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            columnInvalidLinksUser.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnInvalidLinksUser.Frozen = true;
            gridInvalidLinks.Columns.Add(columnInvalidLinksUser);

            columnInvalidLinksLink = new DataGridViewLinkColumn();
            columnInvalidLinksLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnInvalidLinksLink.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            columnInvalidLinksLink.HeaderText = Resources.strLink;
            columnInvalidLinksLink.Name = "LinkColumn";
            columnInvalidLinksLink.ReadOnly = true;
            columnInvalidLinksLink.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnInvalidLinksLink.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridInvalidLinks.Columns.Add(columnInvalidLinksLink);

            columnInvalidLinksPermission = new DataGridViewTextBoxColumn();
            columnInvalidLinksPermission.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnInvalidLinksPermission.HeaderText = Resources.strAccessible;
            columnInvalidLinksPermission.Name = "PermissionColumn";
            columnInvalidLinksPermission.ReadOnly = true;
            columnInvalidLinksPermission.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnInvalidLinksPermission.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridInvalidLinks.Columns.Add(columnInvalidLinksPermission);

            columnInvalidLinksAction = new DataGridViewLinkColumn();
            columnInvalidLinksAction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnInvalidLinksAction.HeaderText = Resources.strAction;
            columnInvalidLinksAction.Name = "ActionColumn";
            columnInvalidLinksAction.Text = "Action";
            columnInvalidLinksAction.ReadOnly = true;
            columnInvalidLinksAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnInvalidLinksAction.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridInvalidLinks.Columns.Add(columnInvalidLinksAction);
            #endregion

            #region Create Valid Links Grid
            gridValidLinks.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            gridValidLinks.CellContentClick -= new DataGridViewCellEventHandler(grid_CellContentClick);
            gridValidLinks.CellContentClick += new DataGridViewCellEventHandler(grid_CellContentClick);

            columnValidLinksUser = new SpannedDataGridView.DataGridViewTextBoxColumnSpanned();
            columnValidLinksUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnValidLinksUser.HeaderText = Resources.strRecipient;
            columnValidLinksUser.Name = "UserColumn";
            columnValidLinksUser.ReadOnly = true;
            columnValidLinksUser.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnValidLinksUser.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            columnValidLinksUser.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnValidLinksUser.Frozen = true;
            gridValidLinks.Columns.Add(columnValidLinksUser);

            columnValidLinksLink = new DataGridViewLinkColumn();
            columnValidLinksLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnValidLinksLink.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            columnValidLinksLink.HeaderText = Resources.strLink;
            columnValidLinksLink.Name = "LinkColumn";
            columnValidLinksLink.ReadOnly = true;
            columnValidLinksLink.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnValidLinksLink.SortMode = DataGridViewColumnSortMode.NotSortable;

            gridValidLinks.Columns.Add(columnValidLinksLink);

            columnValidLinksPermission = new DataGridViewTextBoxColumn();
            columnValidLinksPermission.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnValidLinksPermission.HeaderText = Resources.strAccessible;
            columnValidLinksPermission.Name = "PermissionColumn";
            columnValidLinksPermission.ReadOnly = true;
            columnValidLinksPermission.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnValidLinksPermission.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridValidLinks.Columns.Add(columnValidLinksPermission);

            columnValidLinksAction = new DataGridViewLinkColumn();
            columnValidLinksAction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnValidLinksAction.HeaderText = Resources.strAction;
            columnValidLinksAction.Name = "ActionColumn";
            columnValidLinksAction.Text = "Action";
            columnValidLinksAction.ReadOnly = true;
            columnValidLinksAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnValidLinksAction.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridValidLinks.Columns.Add(columnValidLinksAction);

            #endregion

            #region Create Unchecked Links Grid
            gridUncheckedLinks.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            gridUncheckedLinks.CellContentClick -= new DataGridViewCellEventHandler(grid_CellContentClick);
            gridUncheckedLinks.CellContentClick += new DataGridViewCellEventHandler(grid_CellContentClick);

            columnUncheckedLinksLink = new DataGridViewLinkColumn();
            columnUncheckedLinksLink.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnUncheckedLinksLink.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            columnUncheckedLinksLink.HeaderText = Resources.strLink;
            columnUncheckedLinksLink.Name = "LinkColumn";
            columnUncheckedLinksLink.ReadOnly = true;
            columnUncheckedLinksLink.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnUncheckedLinksLink.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridUncheckedLinks.Columns.Add(columnUncheckedLinksLink);

            columnUncheckedLinksReason = new DataGridViewTextBoxColumn();
            columnUncheckedLinksReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            columnUncheckedLinksReason.HeaderText = Resources.strReason;
            columnUncheckedLinksReason.Name = "ReasonColumn";
            columnUncheckedLinksReason.ReadOnly = true;
            columnUncheckedLinksReason.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            columnUncheckedLinksReason.SortMode = DataGridViewColumnSortMode.NotSortable;
            gridUncheckedLinks.Columns.Add(columnUncheckedLinksReason);

            #endregion

            _bw = new BackgroundWorker();
        }
        public void DoProcess(bool panelVisible)
        {
            if (_bw.IsBusy || !panelVisible) return;
            _bw.DoWork -= new DoWorkEventHandler(bw_DoWork);
            _bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            _bw.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            _bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            progressBar.MarqueeAnimationSpeed = 30;
            progressBar.Show();
            _bw.RunWorkerAsync();
        }
        private void buttonCheckLinks_Click(object sender, EventArgs e)
        {
            _paneCreated = false;
            DoProcess(true);
        }
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            if (!_paneCreated && ValidateSettings()) ProcessEmail();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _paneCreated = true;
            progressBar.InvokeIfRequired(c => { c.Hide(); });
        }
        private bool ValidateSettings()
        {
            bool result = true;
            if (AddInSettings.CSInstances == null)
            {
                result = false;
            }
            else
            {
                foreach (var instance in AddInSettings.CSInstances)
                {
                    //check any instance specific settings
                    Uri uri = null;
                    if (!Uri.TryCreate(instance.URL, UriKind.Absolute, out uri))
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (!result)
            {
                tabLinks.InvokeIfRequired(c => { c.Visible = false; });
                textStatus.InvokeIfRequired(c =>
                {
                    c.BackColor = textStatus.BackColor;
                    c.ForeColor = System.Drawing.SystemColors.WindowText;
                    c.Text = Resources.strInvalidSettings;
                    c.SuspendLayout();
                });
            }
            return result;
        }
        private void ProcessEmail()
        {
            if (_email == null) return;
            buttonCheckLinks.InvokeIfRequired(c => { c.Enabled = false; });
            System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
            try
            {
                tabLinks.InvokeIfRequired(c => { c.Visible = false; });
                buttonExport.InvokeIfRequired(c => { c.Visible = false; });
                textStatus.InvokeIfRequired(c =>
                {
                    c.BackColor = textStatus.BackColor;
                    c.ForeColor = System.Drawing.SystemColors.WindowText;
                    c.Text = Resources.strValidating;
                    c.SuspendLayout();
                });
                List<ItemData> listOfLinks = new List<ItemData>();
                List<UserData> listOfRecipients = new List<UserData>();
                List<MatrixRow> matrixRows = new List<MatrixRow>();
                System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:Links IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
                
                ListDictionary hLinks = new ListDictionary();
              
                Outlook.Inspector inspector = _email.Application.ActiveInspector();
                var doc = inspector.WordEditor;
                string emailText = doc.Content.Text;
                foreach (var hlink in doc.Hyperlinks)
                {
                    if (hlink.Address != null)
                    {
                        string link = hlink.Address;
                        if (link.Substring(link.Length - 1, 1) == "/") link = link.Remove(link.Length - 1, 1);
                        if (link.StartsWith("http") && !hLinks.Contains(link))
                        {
                            if ( hlink.TextToDisplay != string.Empty )
                            {
                                hLinks.Add(link, hlink.TextToDisplay);
                                emailText = emailText.Replace(hlink.TextToDisplay, string.Empty);
                            }
                            else
                            {
                                hLinks.Add(link, hlink.Address);
                            }
                            
                        }
                    }
                }
                //get all urls
                Regex regex = new Regex(@"(?:\w+):\/\/(?:[\w@][\w.:@]+)\/?[\w\.?=%&=\-@/$,]*");
                foreach (Match match in regex.Matches(emailText))
                {
                    string link = match.Value;
                    if (link.Substring(link.Length - 1, 1) == "/") link = link.Remove(link.Length - 1, 1);
                    if (!hLinks.Contains(link)) hLinks.Add(link, link);
                }
                foreach (string hrefLink in hLinks.Keys)
                {
                    string link = hrefLink.Trim();
                    if (link != string.Empty)
                    {
                        ItemData item = new ItemData();
                        item.item_link = link;
                        if (listOfLinks.Find(l => l.item_link.ToLower() == item.item_link.ToLower()) == null) listOfLinks.Add(item);
                    }
                }
                System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:Links OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
                System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:Recipients IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
                Outlook.Recipients recipients = _email.Recipients;
                if (recipients != null)
                {
                    foreach (Outlook.Recipient recipient in recipients)
                    {
                        GetRecipients(listOfRecipients, ((dynamic)recipient).AddressEntry);

                        if (recipient != null) Marshal.ReleaseComObject(recipient);
                    }

                    if (recipients != null) Marshal.ReleaseComObject(recipients);
                }
                System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:Recipients OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
                if (listOfRecipients.Count == 0 && listOfLinks.Count == 0)
                {
                    textStatus.InvokeIfRequired(c => { c.Text = Resources.strNoRecipientsNoLinks; });
                }
                else if (listOfLinks.Count == 0)
                {
                    textStatus.InvokeIfRequired(c => { c.Text = Resources.strNoLinks; });
                }
                else if (listOfRecipients.Count == 0)
                {
                    textStatus.InvokeIfRequired(c => { c.Text = Resources.strNoRecipients; });
                }

                if (listOfRecipients.Count > 0 &&  listOfLinks.Count > 0)
                {
                    //get links per instance configured in settings
                    ListDictionary linksPerCSInstance = new ListDictionary();
                    List<ItemData> invalidLinks = new List<ItemData>();

                    foreach (AddInSettings.CSInstanceSettings instanceSetting in AddInSettings.CSInstances)
                    {
                        List<ItemData> links = new List<ItemData>();
                        Uri instanceUri = new Uri(instanceSetting.URL);
                        foreach (ItemData link in listOfLinks)
                        {
                            //Uri linkUri = new Uri(link.item_link);
                            Uri linkUri = null;
                            if (!Uri.TryCreate(link.item_link, UriKind.Absolute, out linkUri))
                            {
                                if (invalidLinks.Find(l => l.item_link == link.item_link) == null) invalidLinks.Add(link);
                            }
                            else
                            {
                                if (instanceUri.Host.ToLower() == linkUri.Host.ToLower())
                                {
                                    if (linkUri.AbsoluteUri.ToLower().StartsWith(instanceUri.AbsoluteUri.ToLower()))
                                    {
                                        links.Add(link);
                                    }
                                    else
                                    {
                                        if (invalidLinks.Find(l => l.item_link == link.item_link) == null) invalidLinks.Add(link);
                                    }
                                }
                                else
                                {
                                    if (invalidLinks.Find(l => l.item_link == link.item_link) == null) invalidLinks.Add(link);
                                }
                            }
                        }
                        linksPerCSInstance.Add(instanceSetting, links);
                    }

                    //call each instance
                    IDictionaryEnumerator en = linksPerCSInstance.GetEnumerator();
                    while (en.MoveNext())
                    {
                        List<MatrixRow> checkedLinks = null;
                        List<ItemData> linksToCheck = en.Value as List<ItemData>;
                        foreach (ItemData item in linksToCheck)
                        {
                            invalidLinks.Remove(item);
                        }
                        AddInSettings.CSInstanceSettings instanceSettings = en.Key as AddInSettings.CSInstanceSettings;
                        if (linksToCheck != null && linksToCheck.Count > 0 && listOfRecipients != null && listOfRecipients.Count > 0)
                        {
                            checkedLinks = CheckLinks(instanceSettings, linksToCheck, listOfRecipients);
                        }
                        if (checkedLinks != null) matrixRows.AddRange(checkedLinks);
                    }

                    //sort out invalid links
                    foreach (ItemData il in invalidLinks)
                    {
                        MatrixRow invalidLink = matrixRows.Find(mr => mr.Link == il.item_link);
                        if (invalidLink == null)
                        {
                            MatrixRow row = new MatrixRow();
                            row.Link = il.item_link;
                            row.Unchecked = true;
                            row.Message = Resources.strNoMatchCSInstance;
                            matrixRows.Add(row);
                        }
                    }

                    matrixRows = matrixRows.OrderBy(o => o.User).ToList();

                    System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:ProcessGrids IN {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
                    string currentUserInvalidLinks = string.Empty;
                    string currentUserValidLinks = string.Empty;
                    string currentLinkUncheckedLinks = string.Empty;
                    int currentUserInvalidLinksCellIndex = 0;
                    int currentUserValidLinksCellIndex = 0;
                    int currentLinkUncheckedLinksCellIndex = 0;
                    int indexInvalidLinks = 0;
                    int indexValidLinks = 0;
                    int indexUncheckedLinks = 0;
                    int indexValidUsers = -1;
                    int indexInvalidUsers = -1;
                    gridInvalidLinks.InvokeIfRequired(c => { c.Rows.Clear(); });
                    gridValidLinks.InvokeIfRequired(c => { c.Rows.Clear(); });
                    gridUncheckedLinks.InvokeIfRequired(c => { c.Rows.Clear(); });
                    foreach (MatrixRow mRow in matrixRows)
                    {
                        if (mRow.Unchecked) //UNCHECKED
                        {
                            DataGridViewRow gridRow = null;
                            gridUncheckedLinks.InvokeIfRequired(c =>
                            {
                                indexUncheckedLinks = c.Rows.Add();
                                gridRow = c.Rows[indexUncheckedLinks];
                            });
                            if (currentLinkUncheckedLinks != mRow.Link)
                            {
                                currentLinkUncheckedLinks = mRow.Link;
                                currentLinkUncheckedLinksCellIndex = indexUncheckedLinks;
                                gridRow.Cells[columnUncheckedLinksLink.Name].Value = mRow.Link;
                            }
                            ((DataGridViewLinkCell)gridRow.Cells[columnUncheckedLinksLink.Name]).Value = hLinks[mRow.Link] != null ? hLinks[mRow.Link].ToString() : mRow.Link;
                            ((DataGridViewLinkCell)gridRow.Cells[columnUncheckedLinksLink.Name]).ToolTipText = mRow.Link;
                            ((DataGridViewTextBoxCell)gridRow.Cells[columnUncheckedLinksReason.Name]).Value = mRow.Message;
                            gridRow.DefaultCellStyle.BackColor = indexUncheckedLinks % 2 == 0 ? System.Drawing.SystemColors.ControlLightLight : System.Drawing.SystemColors.ControlLight;
                        }
                        else
                        {
                            if (mRow.HasAccess) //VALID
                            {
                                DataGridViewRow gridRow = null;
                                gridValidLinks.InvokeIfRequired(c =>
                                {
                                    indexValidLinks = c.Rows.Add();
                                    gridRow = c.Rows[indexValidLinks];
                                });
                                if (currentUserValidLinks != mRow.User)
                                {
                                    indexValidUsers += 1;
                                    currentUserValidLinks = mRow.User;
                                    currentUserValidLinksCellIndex = indexValidLinks;
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridRow.Cells[columnValidLinksUser.Name]).Value = mRow.User;
                                }
                                else
                                {
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridRow.Cells[columnValidLinksUser.Name]).Value = string.Empty;
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridValidLinks[gridValidLinks.Columns[columnValidLinksUser.Name].Index, currentUserValidLinksCellIndex]).RowSpan += 1;

                                }
                                ((DataGridViewLinkCell)gridRow.Cells[columnValidLinksLink.Name]).Value = hLinks[mRow.Link] != null ? hLinks[mRow.Link].ToString() : mRow.Link;
                                ((DataGridViewLinkCell)gridRow.Cells[columnValidLinksLink.Name]).ToolTipText = mRow.Link;
                                ((DataGridViewTextBoxCell)gridRow.Cells[columnValidLinksPermission.Name]).Value = string.Format("Yes(Permissions:{0})", mRow.Permissions);
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksAction.Name]).Value = Resources.strAction;
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksAction.Name]).ToolTipText = mRow.Action;
                                gridRow.DefaultCellStyle.BackColor = indexValidUsers % 2 == 0 ? System.Drawing.SystemColors.ControlLightLight : System.Drawing.SystemColors.ControlLight;
                            }
                            else //INVALID
                            {
                                DataGridViewRow gridRow = null;
                                gridInvalidLinks.InvokeIfRequired(c =>
                                {
                                    indexInvalidLinks = c.Rows.Add();
                                    gridRow = c.Rows[indexInvalidLinks];
                                });
                                if (currentUserInvalidLinks != mRow.User)
                                {
                                    indexInvalidUsers += 1;
                                    currentUserInvalidLinks = mRow.User;
                                    currentUserInvalidLinksCellIndex = indexInvalidLinks;
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridRow.Cells[columnInvalidLinksUser.Name]).Value = mRow.User;
                                }
                                else
                                {
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridRow.Cells[columnInvalidLinksUser.Name]).Value = string.Empty;
                                    ((SpannedDataGridView.DataGridViewTextBoxCellSpanned)gridInvalidLinks[gridInvalidLinks.Columns[columnInvalidLinksUser.Name].Index, currentUserInvalidLinksCellIndex]).RowSpan += 1;
                                }
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksLink.Name]).Value = hLinks[mRow.Link] != null ? hLinks[mRow.Link].ToString() : mRow.Link;
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksLink.Name]).ToolTipText = mRow.Link;
                                ((DataGridViewTextBoxCell)gridRow.Cells[columnInvalidLinksPermission.Name]).Value = mRow.Message;
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksAction.Name]).Value = Resources.strAction;
                                ((DataGridViewLinkCell)gridRow.Cells[columnInvalidLinksAction.Name]).ToolTipText = mRow.Action;
                                gridRow.DefaultCellStyle.BackColor = indexInvalidUsers % 2 == 0 ? System.Drawing.SystemColors.ControlLightLight : System.Drawing.SystemColors.ControlLight;
                            }
                        }
                    }
                    System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail:ProcessGrids OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));

                    if (tabLinks.TabPages.Contains(tabValid)) tabLinks.InvokeIfRequired(c => { c.TabPages.Remove(tabValid); });
                    if (tabLinks.TabPages.Contains(tabInvalid)) tabLinks.InvokeIfRequired(c => { c.TabPages.Remove(tabInvalid); });
                    if (tabLinks.TabPages.Contains(tabUnchecked)) tabLinks.InvokeIfRequired(c => { c.TabPages.Remove(tabUnchecked); });


                    if (gridInvalidLinks.Rows.Count > 0)
                    {
                        tabLinks.InvokeIfRequired(c => { c.TabPages.Insert(0, tabInvalid); });
                        textStatus.InvokeIfRequired(c =>
                        {
                            c.Text = Resources.strInvalidLinksInvalidRecipients;
                            c.ForeColor = System.Drawing.Color.Red;
                        });
                    }
                    if (gridValidLinks.Rows.Count > 0)
                    {
                        tabLinks.InvokeIfRequired(c => { c.TabPages.Insert(tabLinks.TabCount == 0 ? 0 : 1, tabValid); });
                        textStatus.InvokeIfRequired(c =>
                        {
                            c.Text = Resources.strValidLinksValidRecipients;
                            c.ForeColor = System.Drawing.Color.Green;
                        });
                    }
                    if (gridUncheckedLinks.Rows.Count > 0)
                    {
                        tabLinks.InvokeIfRequired(c => { c.TabPages.Insert(tabLinks.TabCount == 0 ? 0 : tabLinks.TabCount, tabUnchecked); });
                        textStatus.InvokeIfRequired(c =>
                        {
                            c.Text = Resources.strUncheckedLinksUncheckedRecipients;
                            c.ForeColor = System.Drawing.Color.Red;
                        });
                    }

                    if (tabLinks.TabPages.Count >= 2)
                    {
                        textStatus.InvokeIfRequired(c => { c.Text = string.Empty; });
                    }

                    buttonExport.InvokeIfRequired(c => { c.Visible = gridInvalidLinks.Rows.Count > 0 || gridValidLinks.Rows.Count > 0 || gridUncheckedLinks.Rows.Count > 0 ? true : false; });
                    tabLinks.InvokeIfRequired(c => { c.Visible = true; });
                    if (tabLinks.TabPages.Count > 0) DisplayGrids(tabLinks.TabPages[0]);
                    textStatus.InvokeIfRequired(c => { c.ResumeLayout(); });
                }
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                textStatus.InvokeIfRequired(c =>
                {
                    c.Text = string.Format("Error: {0}{1}", ex.Message, ex.StackTrace != null && ex.StackTrace.Length > ex.StackTrace.LastIndexOf(":line") && ex.StackTrace.LastIndexOf(":line") > 0 ? ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(":line"), ex.StackTrace.Length - ex.StackTrace.LastIndexOf(":line")) : string.Empty);
                    c.ResumeLayout();
                });
            }
            buttonCheckLinks.InvokeIfRequired(c => { c.Enabled = true; });
            System.Diagnostics.Debug.WriteLine(string.Format("ProcessEmail OUT {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff")));
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0) return;
            string link = null;
            if (((DataGridView)sender).Name == gridInvalidLinks.Name)
            {
                if (e.ColumnIndex == 1 || e.ColumnIndex == 3)
                {
                    link = gridInvalidLinks[e.ColumnIndex, e.RowIndex].ToolTipText;
                }
            }
            else if (((DataGridView)sender).Name == gridValidLinks.Name)
            {
                if (e.ColumnIndex == 1 || e.ColumnIndex == 3)
                {
                    link = gridValidLinks[e.ColumnIndex, e.RowIndex].ToolTipText;
                }
            }
            else if (((DataGridView)sender).Name == gridUncheckedLinks.Name)
            {
                if (e.ColumnIndex == 0)
                {
                    link = gridUncheckedLinks[e.ColumnIndex, e.RowIndex].ToolTipText;
                }
            }

            if (link != null)
            {
                try
                {
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.EnableRaisingEvents = false;
                    proc.StartInfo.FileName = link;
                    proc.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Could not start process: {0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool ContainsColumn(string name)
        {
            bool result = false;
            foreach (DataGridViewColumn col in gridInvalidLinks.Columns)
            {
                if (col.HeaderText == name)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private string GetColumnName(string header)
        {
            string result = string.Empty;
            foreach (DataGridViewColumn col in gridInvalidLinks.Columns)
            {
                if (col.HeaderText == header)
                {
                    result = col.Name;
                    break;
                }
            }
            return result;
        }

        private void GetRecipients(List<UserData> listOfRecipients, Microsoft.Office.Interop.Outlook.AddressEntry addressEntry)
        {
            if (addressEntry != null)
            {
                if (addressEntry.Members != null)
                {
                    foreach (Microsoft.Office.Interop.Outlook.AddressEntry ae in addressEntry.Members)
                    {
                        UserData user = GetRecipient(ae);
                        if (user != null && listOfRecipients.Find(r => r.user_email == user.user_email) == null) listOfRecipients.Add(user);

                        if (ae.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeDistributionListAddressEntry)
                        {
                            GetRecipients(listOfRecipients, ae);
                        }
                        if (ae != null) Marshal.ReleaseComObject(ae);
                    }
                }
                else
                {
                    UserData user = GetRecipient(addressEntry);
                    if (user != null && listOfRecipients.Find(r => r.user_email == user.user_email) == null) listOfRecipients.Add(user);
                }
                if (addressEntry != null) Marshal.ReleaseComObject(addressEntry);
            }
        }

        private UserData GetRecipient(Microsoft.Office.Interop.Outlook.AddressEntry addressEntry)
        {
            UserData result = null;
            switch (addressEntry.AddressEntryUserType)
            {
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangeAgentAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangeDistributionListAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olOutlookDistributionListAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangeOrganizationAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangePublicFolderAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry:
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry:
                    Outlook.ExchangeUser exchangeUser = addressEntry.GetExchangeUser();
                    if (exchangeUser != null)
                    {
                        result = new UserData();
                        result.user_email = exchangeUser.PrimarySmtpAddress;
                        result.user_name = exchangeUser.Alias;
                        result.user_first_name = exchangeUser.FirstName;
                        result.user_last_name = exchangeUser.LastName;
                        Marshal.ReleaseComObject(exchangeUser);
                    }
                    break;
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olLdapAddressEntry:
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olOtherAddressEntry:
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olOutlookContactAddressEntry:
                case Microsoft.Office.Interop.Outlook.OlAddressEntryUserType.olSmtpAddressEntry:
                    result = new UserData();
                    result.user_email = addressEntry.Address;
                    break;
                default:
                    break;
            }

            return result;
        }

        private List<MatrixRow> CheckLinks(AddInSettings.CSInstanceSettings settings, List<ItemData> listOfLinks, List<UserData> listOfRecipients)
        {
            List<MatrixRow> result = null;

            if (settings == null || listOfLinks == null || listOfLinks.Count == 0 || listOfRecipients == null || listOfRecipients.Count == 0) return result;

            result = new List<MatrixRow>();

            //string token = CSHelper.Login(instanceURL, instanceAuthentication);
            //string token = CSHelper.HttpPost(@"http://fcscs105/otcs/cs.exe/api/v1/auth", new string[] { "username", "password" }, new string[] { "Admin", "Fastman1" });
            //token = token.Replace("{\"ticket\":\"", string.Empty).Replace("\"}", string.Empty);
            //InfoResult result = CSHelper.HttpGet(string.Format(@"{0}/api/v1/nodes/{1}", Properties.Settings.Default.CSURL, nodeID), token, typeof(InfoResult));
            //n = string.Format("{0}{1}{2}", n, Environment.NewLine, result.data.name);
            RequestInfo requestInfo = new RequestInfo();

            requestInfo.items = listOfLinks.ToArray();
            requestInfo.users = listOfRecipients.ToArray();
            //MessageBox.Show(String.Format("The following locale IDs are registered for this application:{0}Install Language - {1}{0}User Interface Language - {2}{0}Help Language - {3}", Environment.NewLine, _email.Application.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDInstall),_email.Application.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDUI), _email.Application.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDHelp)));
            System.Globalization.CultureInfo currentUICulture = new System.Globalization.CultureInfo(_email.Application.LanguageSettings.get_LanguageID(Office.MsoAppLanguageID.msoLanguageIDUI));//
            requestInfo.language = string.Format("_{0}", currentUICulture.Name.Replace("-", "_"));
            dynamic responseInfo = null;
            string errMsg = null;
            try
            {
                responseInfo = CSHelper.FcsEffectivePerms(settings.URL, typeof(RequestInfo), typeof(ResponseInfo), requestInfo, settings.Authentication, settings.Timeout);
                if (!(responseInfo is ResponseInfo) && responseInfo is string) errMsg = responseInfo != string.Empty ? responseInfo : "Blank response";
            }
            catch (Exception ex)
            {
                errMsg = string.Format("Error: {0}", ex.Message);
            }
            //TODO: validation - no links, no users etc.
            if (responseInfo == null || !(responseInfo is ResponseInfo) || responseInfo.response_rows == null || responseInfo.response_rows.Length == 0)
            {
                foreach (ItemData link in listOfLinks)
                {
                    MatrixRow row = new MatrixRow();
                    row.Link = link.item_link;
                    row.Unchecked = true;
                    row.Message = errMsg;
                    result.Add(row);
                }
                return result;
            }

            foreach (ResponseRow responseRow in responseInfo.response_rows)
            {
                MatrixRow row = new MatrixRow();
                row.Link = responseRow.item.item_link;
                row.HasAccess = responseRow.permission.has_access;
                row.Unchecked = false;
                row.Permissions = responseRow.permission.permissions;
                //TODO: all the permissions logic if any handled here in the client
                //if (!row.Permission) row.Message = responseRow.permission.return_message != null && responseRow.permission.return_message != string.Empty ? responseRow.permission.return_message : "Insufficient permissions";
                row.Message = responseRow.permission.return_message;
                row.Action = settings.Action.Replace("{$csurl}", settings.URL).Replace("{$nodeid}", responseRow.item.item_id.ToString()).Replace("{$userid}", responseRow.user.user_id.ToString());
                row.User = string.Format("{1} {2}{0}<{3}>", Environment.NewLine, responseRow.user.user_first_name, responseRow.user.user_last_name, responseRow.user.user_email);
                //if (responseRow.user.user_found)
                //{
                //    row.User = string.Format("{1} {2}{0}<{3}>", Environment.NewLine, responseRow.user.user_first_name, responseRow.user.user_last_name, responseRow.user.user_email);
                //}
                //else
                //{
                //    if(responseRow.user.user_email != null) row.User = string.Format("User with email <{0}> not found in Content Server.", responseRow.user.user_email);
                //    if (responseRow.permission.return_message == null)
                //    {
                //        row.Message = "User not found in Content Server.";
                //    }
                //}
                result.Add(row);
            }

            return result;
        }

        //Random rand = new Random();
        //private bool GetRandomPermission()
        //{
        //    double trueProbability = 0.6;
        //    bool result = rand.NextDouble() < trueProbability;
        //    return result;
        //}

        private void tabLinks_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index > tabLinks.TabPages.Count - 1) return;
            TabPage tabToDraw = tabLinks.TabPages[e.Index];
            if (e.State == DrawItemState.Selected)
            {
                e.Graphics.FillRectangle(new SolidBrush(SystemColors.ControlLightLight), e.Bounds);
            }
            else
            {
                e.Graphics.FillRectangle(new SolidBrush(SystemColors.ControlLight), e.Bounds);
            }

            Rectangle paddedBounds = e.Bounds;
            int yOffset = (e.State == DrawItemState.Selected) ? -2 : 1;
            paddedBounds.Offset(1, yOffset);
            TextRenderer.DrawText(e.Graphics, tabToDraw.Text, Font, paddedBounds, tabToDraw.ForeColor);
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            StringBuilder contents = new StringBuilder();
            DataGridViewRowCollection rows = null;
            DataGridViewColumnCollection columns = null;
            string linksType = string.Empty;
            if (tabLinks.SelectedTab.Name == tabInvalid.Name)
            {
                rows = gridInvalidLinks.Rows;
                columns = gridInvalidLinks.Columns;
                linksType = "invalid";
            }
            else if (tabLinks.SelectedTab.Name == tabValid.Name)
            {
                rows = gridValidLinks.Rows;
                columns = gridValidLinks.Columns;
                linksType = "valid";
            }
            else if (tabLinks.SelectedTab.Name == tabUnchecked.Name)
            {
                rows = gridUncheckedLinks.Rows;
                columns = gridUncheckedLinks.Columns;
                linksType = "unchecked";
            }

            string fileName = System.IO.Path.Combine(System.IO.Path.GetTempPath(), string.Format("fcs_exported_{0}_links_{1}.txt", linksType, DateTime.Now.Ticks));
            string user = string.Empty;
            foreach (DataGridViewRow row in rows)
            {
                if (tabLinks.SelectedTab.Name != tabUnchecked.Name)
                {
                    user = row.Cells["UserColumn"].Value.ToString() != string.Empty ? row.Cells["UserColumn"].Value.ToString() : user;
                }
                foreach (DataGridViewColumn column in columns)
                {
                    string v = column.Name == "UserColumn" ? user.Replace(Environment.NewLine, string.Empty) : row.Cells[column.Name].Value.ToString();
                    contents.AppendFormat("{0}\t", v);
                }
                contents.Append(Environment.NewLine);
            }
            System.IO.File.AppendAllText(fileName, contents.ToString());
            System.Diagnostics.Process.Start(fileName);

        }

        private void tabLinks_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayGrids(tabLinks.SelectedTab);
        }

        private void DisplayGrids(TabPage tabLinksSelectedTab)
        {
            if (tabLinksSelectedTab == null) return;
            bool visible = false;
            if (tabLinksSelectedTab.Name == tabInvalid.Name)
            {
                visible = gridInvalidLinks.Rows.Count > 0 ? true : false;
                gridInvalidLinks.InvokeIfRequired(c => { c.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells); });
                gridInvalidLinks.InvokeIfRequired(c => { c.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells); });
            }
            else if (tabLinksSelectedTab.Name == tabValid.Name)
            {
                visible = gridValidLinks.Rows.Count > 0 ? true : false;
                gridValidLinks.InvokeIfRequired(c => { c.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells); });
                gridValidLinks.InvokeIfRequired(c => { c.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells); });
            }
            else if (tabLinksSelectedTab.Name == tabUnchecked.Name)
            {
                visible = gridUncheckedLinks.Rows.Count > 0 ? true : false;
                gridUncheckedLinks.InvokeIfRequired(c => { c.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells); });
                gridUncheckedLinks.InvokeIfRequired(c => { c.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells); });
            }
            buttonExport.InvokeIfRequired(c => { c.Visible = visible; });

        }
    }
}