﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using Microsoft.Win32;

namespace Fastman.Opentext.ContentServer.OutlookLinksValidator
{
    [ComVisible (true)]
    public partial class AddInOptionsControl : UserControl, Microsoft.Office.Interop.Outlook.PropertyPage
    {
        private const int captionDispID = -518;
        private bool _isDirty = false;
        Microsoft.Office.Interop.Outlook.PropertyPageSite _PropertyPageSite = null;

        public AddInOptionsControl()
        {
            InitializeComponent();
            this.Load += new EventHandler(OptionPage_Load);
        }

        DataTable tableCSInstances = null;
        private class EnumModel
        {
            public int Value { get; set; }
            public string Name { get; set; }
        }

        private void OptionPage_Load(object sender, EventArgs e)
        {
            try
            {
                columnCSAuthentication.DataSource = ((IEnumerable<CSHelper.AuthenticationMode>)Enum.GetValues(typeof(CSHelper.AuthenticationMode))).Select(c => new EnumModel() { Value = (int)c, Name = c.ToString() }).ToList();
                columnCSAuthentication.ValueMember = "Value";
                columnCSAuthentication.DisplayMember = "Name";

                // Load our Settings here
                tableCSInstances = new DataTable("CSInstances");

                DataColumn dtCol0 = new DataColumn("Name");
                dtCol0.DataType = System.Type.GetType("System.Int32");
                dtCol0.AutoIncrement = true;
                dtCol0.AutoIncrementStep = 1;
                dtCol0.AutoIncrementSeed = 1;
                dtCol0.Unique = true;
                dtCol0.ReadOnly = true;
                tableCSInstances.Columns.Add(dtCol0);
                tableCSInstances.PrimaryKey = new DataColumn[] { dtCol0 };

                DataColumn dtCol1 = new DataColumn("URL");
                dtCol1.DataType = System.Type.GetType("System.String");
                dtCol1.DefaultValue = @"http://csserver/otcs/cs.exe";
                tableCSInstances.Columns.Add(dtCol1);

                DataColumn dtCol2 = new DataColumn("Authentication");
                dtCol2.DataType = System.Type.GetType("System.Int32");
                dtCol2.DefaultValue = CSHelper.AuthenticationMode.CS_PROMPT;
                tableCSInstances.Columns.Add(dtCol2);

                DataColumn dtCol3 = new DataColumn("Timeout");
                dtCol3.DataType = System.Type.GetType("System.Int32");
                dtCol3.DefaultValue = 5000;
                tableCSInstances.Columns.Add(dtCol3);

                DataColumn dtCol4 = new DataColumn("Action");
                dtCol4.DataType = System.Type.GetType("System.String");
                dtCol4.DefaultValue = @"{$csurl}/properties/{$nodeid}";
                tableCSInstances.Columns.Add(dtCol4);

                foreach (AddInSettings.CSInstanceSettings csInstance in AddInSettings.CSInstances)
                {
                    DataRow row = tableCSInstances.NewRow();
                    row["Name"] = csInstance.Name;
                    row["URL"] = csInstance.URL;
                    row["Authentication"] = csInstance.Authentication;
                    row["Timeout"] = csInstance.Timeout;
                    row["Action"] = csInstance.Action;
                    tableCSInstances.Rows.Add(row);
                }
                //if (Properties.Settings.Default.CSInstances != null)
                //{
                //    foreach (string instanceSetting in Properties.Settings.Default.CSInstances)
                //    {
                //        string[] setting = instanceSetting.Split("|".ToCharArray());
                //        if (setting.Length == 2)
                //        {
                //            DataRow row = tableCSInstances.NewRow();
                //            row["URL"] = setting[0];
                //            row["Authentication"] = setting[1] == string.Empty ? 1 : Convert.ToInt32(setting[1]);
                //            tableCSInstances.Rows.Add(row);
                //        }
                //    }
                //}
                gridCSInstances.DataSource = tableCSInstances;
                tableCSInstances.RowChanged -= new DataRowChangeEventHandler(tableCSInstances_RowChanged);
                tableCSInstances.RowChanged += new DataRowChangeEventHandler(tableCSInstances_RowChanged);
                tableCSInstances.RowDeleted -= new DataRowChangeEventHandler(tableCSInstances_RowChanged);
                tableCSInstances.RowDeleted += new DataRowChangeEventHandler(tableCSInstances_RowChanged);
                gridCSInstances.CellValidating -= new DataGridViewCellValidatingEventHandler(gridCSInstances_CellValidating);
                gridCSInstances.CellValidating += new DataGridViewCellValidatingEventHandler(gridCSInstances_CellValidating);
                // Get our Parent PropertyPageSite Object and store it.
                _PropertyPageSite = GetPropertyPageSite();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error reading settings: " + ex.Message, "Fastman Permissions Manager for Outlook");
            }
        }

        void tableCSInstances_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            OnDirty(true);
        }

        Microsoft.Office.Interop.Outlook.PropertyPageSite GetPropertyPageSite()
        {
            Type type = typeof(System.Object);
            string assembly = type.Assembly.CodeBase.Replace("mscorlib.dll", "System.Windows.Forms.dll");
            assembly = assembly.Replace("file:///", "");

            string assemblyName = System.Reflection.AssemblyName.GetAssemblyName(assembly).FullName;
            Type unsafeNativeMethods = Type.GetType(System.Reflection.Assembly.CreateQualifiedName(assemblyName, "System.Windows.Forms.UnsafeNativeMethods"));

            Type oleObj = unsafeNativeMethods.GetNestedType("IOleObject");
            System.Reflection.MethodInfo methodInfo = oleObj.GetMethod("GetClientSite");
            object propertyPageSite = methodInfo.Invoke(this, null);
            return (Microsoft.Office.Interop.Outlook.PropertyPageSite)propertyPageSite;
        }

        void Microsoft.Office.Interop.Outlook.PropertyPage.Apply()
        {
            if (_isDirty)
            {
                try
                {
                    // Save Settings
                    List<AddInSettings.CSInstanceSettings> csInstances = new List<AddInSettings.CSInstanceSettings>();
                    foreach (DataRow row in tableCSInstances.Rows)
                    {
                        AddInSettings.CSInstanceSettings csInstance = new AddInSettings.CSInstanceSettings();
                        csInstance.Name = row["Name"].ToString();
                        csInstance.URL = row["URL"].ToString();
                        csInstance.Authentication = (CSHelper.AuthenticationMode)row["Authentication"];
                        csInstance.Timeout = row["Timeout"]!= null && row["Timeout"].ToString() != string.Empty ? Convert.ToInt32(row["Timeout"]) : 5000;
                        csInstance.Action = row["Action"].ToString() ;
                        csInstances.Add(csInstance);
                    }
                    AddInSettings.CSInstances = csInstances;

                    //Properties.Settings.Default.CSInstances = new System.Collections.Specialized.StringCollection();
                    //foreach (DataRow row in tableCSInstances.Rows)
                    //{
                    //    Properties.Settings.Default.CSInstances.Add(string.Format("{0}|{1}", row["URL"], row["Authentication"]));
                    //}
                    //Properties.Settings.Default.Save();

                    // Reset Dirty Flag
                    OnDirty(false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error reading settings: " + ex.Message, "Fastman Permissions Manager for Outlook");
                }
            }
        }

        bool Microsoft.Office.Interop.Outlook.PropertyPage.Dirty
        {
            get
            {
                return _isDirty;
            }
        }

        void Microsoft.Office.Interop.Outlook.PropertyPage.GetPageInfo(ref string helpFile, ref int helpContext)
        {
            // TODO: Implement Helpfile
        }
        
        private void OnDirty(bool isDirty)
        {
            if (isDirty == _isDirty) return;

            _isDirty = isDirty;

            // When this Method is called, the PageSite checks for Dirty Flag of all Optionspages.
            if(_PropertyPageSite != null) _PropertyPageSite.OnStatusChange();
        }

        [DispId(captionDispID)]
        public string PageCaption
        {
            get
            {
                return "Content Server Connection";
            }
        }

        private void gridCSInstances_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string errorMessage = string.Empty;

            object v = e.FormattedValue;
            switch (e.ColumnIndex)
            {
                case 0:
                    break;
                case 1: //URL
                    if (v == null || v.ToString().Trim() == string.Empty)
                    {
                        errorMessage = string.Format("{0}{1}{2}", errorMessage, "URL cannot be blank!", Environment.NewLine);
                    }
                    else
                    {
                        Uri uri = null;
                        if (Uri.TryCreate(v.ToString(), UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps))
                        {
                            //URL is valid
                        }
                        else
                        {
                            errorMessage = string.Format("{0}{1}{2}", errorMessage, "URL is not valid!", Environment.NewLine);
                        }
                    }
                    break;
                case 2: //Athentication
                    if (v == null || v.ToString().Trim() == string.Empty)
                    {
                        errorMessage = string.Format("{0}{1}{2}", errorMessage, "Authentication cannot be blank!", Environment.NewLine);
                    }
                    break;
                default:
                    break;
            }

            if (errorMessage != string.Empty)
            {
                MessageBox.Show(string.Format("Invalid settings.{0}{1}", Environment.NewLine, errorMessage), "Fastman Permissions Manager for Outlook");
                e.Cancel = true;
            }
        }
    }
}
