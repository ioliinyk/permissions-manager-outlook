﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    [System.Runtime.Serialization.DataContract]
    public class AuthenticationInfo
    {
        [System.Runtime.Serialization.DataMember(Name = "ticket")]
        public string ticket { get; set; }
    }

}
