﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.ServiceModel;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace ConsoleApplication1
{
    class Program
    {
        public static SearchResult FindAccountByEmail(string email)
        {
            string filter = string.Format("(proxyaddresses=SMTP:{0})", email);

            using (DirectoryEntry gc = new DirectoryEntry("GC:"))
            {
                foreach (DirectoryEntry z in gc.Children)
                {
                    using (DirectoryEntry root = z)
                    {
                        using (DirectorySearcher searcher = new DirectorySearcher(root, filter, new string[] { "proxyAddresses", "objectGuid", "displayName", "distinguishedName" }))
                        {
                            searcher.ReferralChasing = ReferralChasingOption.All;
                            SearchResult result = searcher.FindOne();

                            return result;
                        }
                    }
                    break;
                }
            }

            return null;
        }

        static void Main(string[] args)
        {
            using (var context = new PrincipalContext(ContextType.Domain, "FASTMAN"))
            {
                using (var group = UserPrincipal.FindByIdentity(context, "david.henshaw"))
                {

                    if (group == null)
                    {
                        //MessageBox.Show("Group does not exist");
                    }
                    else
                    {

                        ////var users = group.GetMembers(true);
                        //foreach (UserPrincipal user in users)
                        //{


                        //    //user variable has the details about the user 

                        //}
                    }
                }
            }
            //DirectoryEntry adroot = new DirectoryEntry("LDAP://" + Environment.UserDomainName,"user", "password", AuthenticationTypes.Secure);

            //Get the username and domain information
            string userName = Environment.UserName;
            string domainName = Environment.UserDomainName;

            //Set the correct format for the AD query and filter
            string ldapQueryFormat = @"LDAP://{0}.com/DC={0},DC=com";
            string rootQuery = string.Format(ldapQueryFormat, "yourdomain.com");
            string queryFilterFormat = @"(&(samAccountName={0})(objectCategory=person)(objectClass=user))";
            string searchFilter = string.Format(queryFilterFormat, "John Smith");

            SearchResult result = null;
            using (DirectoryEntry root = new DirectoryEntry(@"LDAP://FASTMAN"))//rootQuery))
            {
                using (DirectorySearcher searcher = new DirectorySearcher(root))
                {
                    searcher.Filter = searchFilter;
                    SearchResultCollection results = searcher.FindAll();

                    result = (results.Count != 0) ? results[0] : null;
                }
            }
            //DirectorySearcher search = new DirectorySearcher("(&(mail=*))");
            //search.PageSize = 1000;
            //search.PropertiesToLoad.Add("mail");
            //foreach (SearchResult result in search.FindAll())
            //{
            //    DirectoryEntry entry = result.GetDirectoryEntry();
            //    Console.WriteLine(entry.Properties["mail"].Value);
            //}
            //SearchResult result = FindAccountByEmail("david.henshaw@fastman.com");

            //string distinguishedName = result.Properties["distinguishedName"][0] as string;
            //string name = result.Properties["displayName"] != null
            //                ? result.Properties["displayName"][0] as string
            //                : string.Empty;
            //Guid adGuid = new Guid((byte[])(result.Properties["objectGUID"][0]));

            //string emailAddress;
            //var emailAddresses = (from string z in result.Properties["proxyAddresses"]
            //                      where z.StartsWith("SMTP")
            //                      select z);
            //emailAddress = emailAddresses.Count() > 0 ? emailAddresses.First().Remove(0, 5) : string.Empty;


            //Console.WriteLine(string.Format("{1}{0}\t{2}{0}\t{3}{0}\t{4}",
            //              Environment.NewLine,
            //              name,
            //              distinguishedName,
            //              adGuid,
            //              emailAddress));
        }
        ////[DllImport("wininet.dll", SetLastError = true)]
        ////public static extern bool InternetGetCookie(string url, string cookieName, StringBuilder cookieData, ref int size);

        //static void Main(string[] args)
        //{
        //    //System.Diagnostics.Process proc = new System.Diagnostics.Process();
        //    //proc.EnableRaisingEvents = false;
        //    //proc.StartInfo.FileName = e.LinkText;
        //    //proc.Start();
        //    //Retrieving System/IE Cookies From C#
        //    //.NET's System.Net.HttpWebRequest doesn't observe system cookies (I'm not a Windows developer; I don't know the canonical name for these - the place where IE sticks its cookies) when making HTTP requests. This is unfortunate if you'd like your application to share a session with another authenticated application which may have run at some earlier point on the same machine. Here's a method which retrieves a cookie for a given host from the IE cookie store, by calling out to the WinInet DLL.

        //    //using System.Runtime.InteropServices;

        //    //...
        //    //[DllImport("wininet.dll", SetLastError=true)]
        //    //public static extern bool InternetGetCookie(
        //    //    string url, string cookieName, StringBuilder cookieData, ref int size);

        //    //public String GetIECookie(Uri uri) {
        //    //    int cookieLength = 256;
        //    //    StringBuilder cookie = new StringBuilder(cookieLength);
        //    //    if (!InternetGetCookie(uri.ToString(), null, cookie, ref cookieLength)) {
        //    //        if (cookieLength < 0) {
        //    //            return null;
        //    //        }
        //    //        cookie = new StringBuilder(cookieLength);
        //    //        if (!InternetGetCookie(uri.ToString(), null, cookie, ref cookieLength)) {
        //    //            return null;
        //    //        }
        //    //    }
        //    //    return cookie.ToString();
        //    //}

        //    //You could then do something along the lines of:
        //    //CookieContainer cookies = new CookieContainer();
        //    //String cookie = GetIECookie(new Uri("http://www.db.com/"));
        //    //if (cookie != null && 0 < cookie.length) {
        //    //    cookies.SetCookies(uri, cookie.Replace(';', ','));
        //    //}

        //    //When you make a relevant outgoing HTTP request, be sure to point the HttpWebRequest's CookieContainer property at the right object.
        //    //////////////////////////////////////////////////////////////
        //    //try
        //    //{
        //    //   string otauthtoken = HttpPost(@"http://fcscs105/otcs/cs.exe/api/v1/auth", new string[]{"username","password"}, new string[]{"Admin","Fastman1"});
        //    //   System.Windows.Forms.MessageBox.Show(r);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    System.Windows.Forms.MessageBox.Show(ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        //    //}
        //    //////////////////////////////////////////////////////////////

        //    //string uri = @"http://fcscs105:8080/otdsws/login";
        //    //System.Diagnostics.Process proc = new System.Diagnostics.Process();
        //    //proc.EnableRaisingEvents = false;
        //    //proc.StartInfo.FileName = uri;
        //    //proc.Start();
        //    //Retrieving System/IE Cookies From C#
        //    //.NET's System.Net.HttpWebRequest doesn't observe system cookies (I'm not a Windows developer; I don't know the canonical name for these - the place where IE sticks its cookies) when making HTTP requests. This is unfortunate if you'd like your application to share a session with another authenticated application which may have run at some earlier point on the same machine. Here's a method which retrieves a cookie for a given host from the IE cookie store, by calling out to the WinInet DLL.
        //    ICredentials credentials = new NetworkCredential("dimitar.belyovski", "Fastman31", "FASTMAN"); //CredentialCache.DefaultCredentials;
        //    CookieContainer cookies = new CookieContainer();

        //    //string postData = "func=ll.login&CurrentClientTime=D/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ":" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        //    //string postData = "func=ll.login&Username=" + "Admin" + "&Password=" + "Fastman1" + "&CurrentClientTime=D/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ":" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
        //    //byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(postData);

        //    //call otds authentication ws
        //    Uri uri = new Uri(string.Format(@"http://fcsotds:8080/ot-authws/services/Authentication?wsdl"));
        //    OTDSAuthentication.AuthenticationClient otdsClient = new OTDSAuthentication.AuthenticationClient();
        //    otdsClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(uri.ToString());

        //    (otdsClient.Endpoint.Binding as BasicHttpBinding).Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
        //    (otdsClient.Endpoint.Binding as BasicHttpBinding).Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;

        //    OTDSAuthentication.OTAuthentication OTDSAuthHeader = new OTDSAuthentication.OTAuthentication();
            

        //    string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        //    username = "dimitar.belyovski@fastman.com"; //username.Substring(username.IndexOf(@"\") + 1);
        //    string otcssso = otdsClient.AuthenticateCurrentUser(ref OTDSAuthHeader);
          


        //    HttpWebRequest request = WebRequest.Create(@"http://xecm/otcs/cs.exe") as HttpWebRequest;
        //    request.PreAuthenticate = true;
        //    request.KeepAlive = true;
        //    request.CookieContainer = cookies;
        //    request.Credentials = credentials;
        //    request.AllowAutoRedirect = true;
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.ServicePoint.Expect100Continue = false;
        //    request.Method = "GET";
        //    request.Timeout = 10000;
        //    request.Headers.Add("OTCSSSO", otcssso.Replace("*OTCSSSO*", string.Empty));
        //    //request.ContentLength = data.Length;
        //    //Stream requestStream = request.GetRequestStream();
        //    //requestStream.Write(data, 0, data.Length);
        //    //requestStream.Close();
        //    request.GetResponse().Close();

        //    string ticket = cookies.GetCookies(new Uri(@"http://xecm/otcs/cs.exe"))["LLCookie"].Value.ToString();
        //    ticket = System.Web.HttpUtility.UrlDecode(ticket);
        //    //AuthenticationInfo token = HttpPost(@"http://cs105/otcs/cs.exe/api/v1/auth", new string[] { "username", "password" }, new string[] { "Admin", "C0nt=nt$=rv=r" });//{ "Admin", "Fastman1" });
        //    //ticket = token.ticket;
        //    //dynamic result = HttpGet(@"http://xecm/otcs/cs.exe/api/v1/nodes/2000", ticket);
        //    HttpWebRequest request1 = WebRequest.Create(@"http://xecm/otcs/cs.exe/api/v1/nodes/59600") as HttpWebRequest;
        //    request1.ContentType = "application/x-www-form-urlencoded";
        //    request1.Headers.Add("OTCSTICKET", ticket);
        //    request1.PreAuthenticate = true;
        //    request1.Credentials = credentials;
        //    request1.CookieContainer = cookies;
        //    //request1.BeginGetResponse(new AsyncCallback(RespCallback), request1);
        //    string result = null;
        //    using (HttpWebResponse response = request1.GetResponse() as HttpWebResponse)
        //    {
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        result = reader.ReadToEnd();
        //    }
        //    Console.ReadLine();
        //}

      
        //private static void RespCallback(IAsyncResult result)
        //{
        //    HttpWebRequest request = (HttpWebRequest)result.AsyncState;
        //    //request.EndGetResponse(result).Close();
        //    HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

        //    Stream responseStream = response.GetResponseStream();
        //    StreamReader reader = new StreamReader(responseStream);
        //    string responseString = reader.ReadToEnd();
        //    reader.Close();
        //    responseStream.Close();
        //    response.Close();
        //}
        //private static string HttpGet(string url, string token)
        //{
        //    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.Headers.Add("OTCSTICKET", token);
        //    request.PreAuthenticate = true;
        //    request.UseDefaultCredentials = true;

        //    string result = null;
        //    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //    {
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        result = reader.ReadToEnd();
        //    }
        //    return result;
        //}

        //private static dynamic HttpPost(string url, string[] paramName, string[] paramVal)
        //{
        //    HttpWebRequest request = WebRequest.Create(new Uri(url)) as HttpWebRequest;
        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.PreAuthenticate = true;
        //    request.UseDefaultCredentials = true;

        //    // Build a string with all the params, properly encoded.
        //    // We assume that the arrays paramName and paramVal are
        //    // of equal length:
        //    StringBuilder paramz = new StringBuilder();
        //    for (int i = 0; i < paramName.Length; i++)
        //    {
        //        paramz.Append(paramName[i]);
        //        paramz.Append("=");
        //        paramz.Append(System.Web.HttpUtility.UrlEncode(paramVal[i]));
        //        paramz.Append("&");
        //    }

        //    // Encode the parameters as form data:
        //    byte[] formData = UTF8Encoding.UTF8.GetBytes(paramz.ToString());
        //    request.ContentLength = formData.Length;

        //    // Send the request:
        //    using (Stream post = request.GetRequestStream())
        //    {
        //        post.Write(formData, 0, formData.Length);
        //    }

        //    // Pick up the response:
        //    dynamic result = null;
        //    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        //    {
        //        System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(AuthenticationInfo));
        //        result = jsonSerializer.ReadObject(response.GetResponseStream());
        //    }

        //    return result;
        //}

        ////public static String GetIECookie(Uri uri, string cookieName)
        ////{
        ////    int cookieLength = 256;
        ////    StringBuilder cookie = new StringBuilder(cookieLength);
        ////    if (!InternetGetCookie(uri.ToString(), cookieName, cookie, ref cookieLength))
        ////    {
        ////        if (cookieLength < 0)
        ////        {
        ////            return null;
        ////        }
        ////        cookie = new StringBuilder(cookieLength);
        ////        if (!InternetGetCookie(uri.ToString(), null, cookie, ref cookieLength))
        ////        {
        ////            return null;
        ////        }
        ////    }
        ////    return cookie.ToString();
        ////}
    }
}
